/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

package eu.lepiller.scabo.scala.ast.ctor;

import eu.lepiller.scabo.scala.ast.Ctor;
import eu.lepiller.scabo.scala.ast.Init;
import eu.lepiller.scabo.scala.ast.Mod;
import eu.lepiller.scabo.scala.ast.Stat;
import eu.lepiller.scabo.scala.ast.term.Param;

import java.util.ArrayList;
import java.util.List;

public class Secondary implements Ctor, Stat {
    public List<Mod> mods;
    public List<List<Param>> paramss;
    public Init init;
    public List<Stat> stats;

    public Secondary() {
        mods = new ArrayList<>();
        paramss = new ArrayList<>();
        stats = new ArrayList<>();
    }

    public void addMod(Mod m) {
        mods.add(m);
    }

    public void addMods(List<Mod> m) {
        for (Mod mod: m) {
            addMod(mod);
        }
    }
}
