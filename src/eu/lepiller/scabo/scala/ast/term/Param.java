/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

package eu.lepiller.scabo.scala.ast.term;

import eu.lepiller.scabo.scala.ast.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Param implements Member {
    public List<Mod> mods;
    public eu.lepiller.scabo.scala.ast.Name name;
    public Type decltype;
    public Term def;

    public Param() {
        mods = new ArrayList<>();
    }

    @Override
    public eu.lepiller.scabo.scala.ast.Name getName() {
        return name;
    }

    @Override
    public String toString() {
        return "(Param "+mods+" "+name+" : "+decltype+" = "+def+")";
    }
}
