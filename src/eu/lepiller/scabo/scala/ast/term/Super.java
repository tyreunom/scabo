/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

package eu.lepiller.scabo.scala.ast.term;

import eu.lepiller.scabo.scala.ast.Name;

public class Super implements Ref {
    public Name thisp, superp;

    @Override
    public String toString() {
        return "Super("+thisp+", "+superp+")";
    }

    @Override
    public boolean equiv(Ref other) {
        if(other instanceof Super) {
            boolean res = true;
            if (thisp != null || ((Super) other).thisp != null) {
                if(thisp == null || ((Super) other).thisp == null)
                    res = false;
                else
                    res = thisp.equiv(((Super) other).thisp);
            }
            if (superp != null || ((Super) other).superp != null) {
                if(superp == null || ((Super) other).superp == null)
                    res = false;
                else
                    res &= superp.equiv(((Super) other).superp);
            }
            return res;
        } else {
            return false;
        }
    }
}
