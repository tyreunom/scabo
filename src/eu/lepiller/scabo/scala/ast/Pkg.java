/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

package eu.lepiller.scabo.scala.ast;

import eu.lepiller.scabo.scala.ast.term.Ref;
import eu.lepiller.scabo.scala.ast.term.Select;

import java.util.ArrayList;
import java.util.List;

public class Pkg extends MemberTerm implements Stat {
    public Ref ref;
    public List<Stat> stats;

    public Pkg() {
        stats = new ArrayList<>();
    }

    @Override
    public Name getName() {
        if(ref instanceof eu.lepiller.scabo.scala.ast.term.Name)
            return (eu.lepiller.scabo.scala.ast.term.Name)ref;
        if(ref instanceof Select)
            return ((Select)ref).name;
        return null;
    }

    @Override
    public String toString() {
        return "package " + ref + "\n" + stats;
    }
}
