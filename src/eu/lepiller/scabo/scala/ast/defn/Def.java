/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

package eu.lepiller.scabo.scala.ast.defn;

import eu.lepiller.scabo.scala.ast.*;
import eu.lepiller.scabo.scala.ast.Type;
import eu.lepiller.scabo.scala.ast.term.Name;
import eu.lepiller.scabo.scala.ast.type.Param;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Def extends MemberTerm implements Defn {
    public List<Mod> mods;
    public List<Param> tparams;
    public List<List<eu.lepiller.scabo.scala.ast.term.Param>> paramss;
    public Type decltype;
    public Term body;

    public Def() {
        mods = new ArrayList<>();
        tparams = new ArrayList<>();
        paramss = new ArrayList<>();
    }

    @Override
    public List<Mod> getMods() {
        return mods;
    }

    @Override
    public void addMod(Mod m) {
        mods.add(m);
    }

    @Override
    public void addMods(List<Mod> m) {
        for (Mod mod: m) {
            addMod(mod);
        }
    }

    @Override
    public String toString() {
        return "FunDef("+getName()+" "+tparams+" ("+paramss+"): "+decltype+" " +
                "= " + body + ")";
    }
}
