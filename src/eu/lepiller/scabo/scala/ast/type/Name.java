/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

package eu.lepiller.scabo.scala.ast.type;

public class Name implements eu.lepiller.scabo.scala.ast.Name, Ref {
    public String value;

    public Name() {}
    public Name(String s) {
        value = s;
    }

    @Override
    public boolean equiv(eu.lepiller.scabo.scala.ast.Name other) {
        if(other instanceof Name)
            return value.compareTo(((Name) other).value) == 0;
        else
            return false;
    }

    @Override
    public String toString() {
        return value;
    }
}
