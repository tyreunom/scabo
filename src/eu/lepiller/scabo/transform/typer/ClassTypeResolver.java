/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

/*
 * This phase is the first phase of the typer. It resoles the most simple type: the type of declared classes. This
 * type is only derived from the declared name and package.
 * The result is the same AST as before, where these types have dots in their name.
 */

package eu.lepiller.scabo.transform.typer;

import eu.lepiller.scabo.scala.ast.Source;
import eu.lepiller.scabo.scala.ast.type.Name;
import eu.lepiller.scabo.scala.ast.Pkg;
import eu.lepiller.scabo.scala.ast.Stat;
import eu.lepiller.scabo.Phase;
import eu.lepiller.scabo.scala.ast.defn.Class;
import eu.lepiller.scabo.scala.ast.defn.Trait;
import eu.lepiller.scabo.scala.ast.defn.Object;

import java.util.ArrayList;
import java.util.List;

public class ClassTypeResolver extends Phase {
    private String currentPackage = "";

    @Override
    public Source visit(Source s) {
        currentPackage = "";
        List<Stat> newStats = new ArrayList<>();
        for(Stat st: s.stats) {
            newStats.add(visit(st));
        }
        s.stats = newStats;
        return s;
    }

    @Override
    public Pkg visit(Pkg p) {
        String ref = p.ref.toString();
        String oldPackage = currentPackage;
        if (currentPackage.isEmpty())
            currentPackage = ref;
        else
            currentPackage = currentPackage + "." + ref;
        for (Stat s : p.stats) {
            Stat ss = visit(s);
        }
        currentPackage = oldPackage;
        return p;
    }

    @Override
    public Class visit(Class c) {
        String name = c.name.toString();
        c.name = new Name(currentPackage + "." + name);
        String oldPackage = currentPackage;
        if (currentPackage.isEmpty())
            currentPackage = name;
        else
            currentPackage = currentPackage + "." + name;
        if(c.template != null) {
            for (Stat s : c.template.stats) {
                Stat ss = visit(s);
            }
        }

        currentPackage = oldPackage;
        return c;
    }

    @Override
    public Trait visit(Trait t) {
        String name = t.name.toString();
        t.name = new Name(currentPackage + "." + name);
        String oldPackage = currentPackage;
        if (currentPackage.isEmpty())
            currentPackage = name;
        else
            currentPackage = currentPackage + "." + name;
        if(t.template != null) {
            for (Stat s : t.template.stats) {
                Stat ss = visit(s);
            }
        }
        currentPackage = oldPackage;
        return t;
    }

    @Override
    public Object visit(Object o) {
        String name = o.name.toString();
        o.name = new eu.lepiller.scabo.scala.ast.term.Name(currentPackage + "." + name);
        String oldPackage = currentPackage;
        if (currentPackage.isEmpty())
            currentPackage = name;
        else
            currentPackage = currentPackage + "." + name;
        if(o.template != null) {
            for (Stat s : o.template.stats) {
                Stat ss = visit(s);
            }
        }
        currentPackage = oldPackage;
        return o;
    }
}
