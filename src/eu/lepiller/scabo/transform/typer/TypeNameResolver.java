/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

/*
 * This phase is the next type resolving phase. When a name is present in the AST, try to resolve its full name.
 */
package eu.lepiller.scabo.transform.typer;

import eu.lepiller.scabo.Phase;
import eu.lepiller.scabo.scala.ast.*;
import eu.lepiller.scabo.scala.ast.defn.Class;
import eu.lepiller.scabo.scala.ast.defn.Object;
import eu.lepiller.scabo.scala.ast.defn.Trait;
import eu.lepiller.scabo.scala.ast.type.Name;
import eu.lepiller.scabo.scala.ast.type.Param;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TypeNameResolver extends Phase {
    private Map<String, String> imported;

    @Override
    public Source visit(Source s) {
        imported = new HashMap<>();

        List<Stat> newStats = new ArrayList<>();
        for(Stat st: s.stats) {
            newStats.add(visit(st));
        }
        s.stats = newStats;
        return s;
    }

    @Override
    public Pkg visit(Pkg p) {
        String ref = p.ref.toString();
        for (Stat s : p.stats) {
            Stat ss = visit(s);
        }
        return p;
    }

    @Override
    public Class visit(Class ctx) {
        Map<String, String> oldImported = imported;

        String name = ctx.name.toString();
        String[] elements = name.split("\\.");
        String[] pkgElements = new String[elements.length-1];
        for(int i=0; i<elements.length-1; i++)
            pkgElements[i] = elements[i];

        String pkg = String.join(".", pkgElements);
        PkgNameLister lister = new PkgNameLister(pkg);
        List<String> content = lister.visit(ast);
        for(String s: content) {
            elements = s.split("\\.");
            imported.put(elements[elements.length-1], s);
        }

        if(ctx.template != null)
            ctx.template = visit(ctx.template);
        ctx.ctor = visit(ctx.ctor);

        List<Param> params = new ArrayList<>();
        for(Param p: ctx.tparams) {
            params.add(visit(p));
        }
        ctx.tparams = params;

        List<Mod> mods = new ArrayList<>();
        for(Mod m: ctx.mods) {
            mods.add(visit(m));
        }
        ctx.mods = mods;

        imported = oldImported;
        return ctx;
    }

    @Override
    public Trait visit(Trait ctx) {
        Map<String, String> oldImported = imported;

        String name = ctx.name.toString();
        String[] elements = name.split("\\.");
        String[] pkgElements = new String[elements.length-1];
        for(int i=0; i<elements.length-1; i++)
            pkgElements[i] = elements[i];

        String pkg = String.join(".", pkgElements);
        PkgNameLister lister = new PkgNameLister(pkg);
        List<String> content = lister.visit(ast);
        for(String s: content) {
            elements = s.split("\\.");
            imported.put(elements[elements.length-1], s);
        }

        if(ctx.template != null)
            ctx.template = visit(ctx.template);
        ctx.ctor = visit(ctx.ctor);

        List<Param> params = new ArrayList<>();
        for(Param p: ctx.tparams) {
            params.add(visit(p));
        }
        ctx.tparams = params;

        List<Mod> mods = new ArrayList<>();
        for(Mod m: ctx.mods) {
            mods.add(visit(m));
        }
        ctx.mods = mods;


        imported = oldImported;
        return ctx;
    }

    @Override
    public Object visit(Object ctx) {
        Map<String, String> oldImported = imported;

        String name = ctx.name.toString();
        String[] elements = name.split("\\.");
        String[] pkgElements = new String[elements.length-1];
        System.arraycopy(elements, 0, pkgElements, 0, pkgElements.length);

        String pkg = String.join(".", pkgElements);
        PkgNameLister lister = new PkgNameLister(pkg);
        List<String> content = lister.visit(ast);
        for(String s: content) {
            elements = s.split("\\.");
            imported.put(elements[elements.length-1], s);
        }

        if(ctx.template != null)
            ctx.template = visit(ctx.template);

        List<Mod> mods = new ArrayList<>();
        for(Mod m: ctx.mods) {
            mods.add(visit(m));
        }
        ctx.mods = mods;

        imported = oldImported;
        return ctx;
    }

    @Override
    public Template visit(Template ctx) {
        ctx.self = visit(ctx.self);
        List<Stat> stats = new ArrayList<>();
        for(Stat st: ctx.stats) {
            stats.add(visit(st));
        }
        ctx.stats = stats;

        List<Init> inits = new ArrayList<>();
        for(Init i: ctx.inits) {
            inits.add(visit(i));
        }
        ctx.inits = inits;

        List<Stat> earlies = new ArrayList<>();
        for(Stat st: ctx.early) {
            earlies.add(visit(st));
        }
        ctx.early = earlies;

        return ctx;
    }

    @Override
    public Init visit(Init ctx) {
        ctx.tpe = visit(ctx.tpe);
        List<List<Term>> argss = new ArrayList<>();
        for(List<Term> ts: ctx.argss) {
            List<Term> terms = new ArrayList<>();
            for(Term t: ts) {
                terms.add(visit(t));
            }
            argss.add(terms);
        }
        ctx.argss = argss;
        return ctx;
    }

    @Override
    public Name visit(Name ctx) {
        return new Name(imported.get(ctx.toString()));
    }
}
