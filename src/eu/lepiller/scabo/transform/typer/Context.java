package eu.lepiller.scabo.transform.typer;

import java.util.Map;

public class Context {
    public String currentPackage;
    public Map<String, String> importedPackages;
}
