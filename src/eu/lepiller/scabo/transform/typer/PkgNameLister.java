/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

package eu.lepiller.scabo.transform.typer;

import eu.lepiller.scabo.AstVisitor;
import eu.lepiller.scabo.scala.ast.Pkg;
import eu.lepiller.scabo.scala.ast.Source;
import eu.lepiller.scabo.scala.ast.Stat;
import eu.lepiller.scabo.scala.ast.defn.Class;
import eu.lepiller.scabo.scala.ast.defn.Object;
import eu.lepiller.scabo.scala.ast.defn.Trait;

import java.util.ArrayList;
import java.util.List;

public class PkgNameLister extends AstVisitor<List<String>> {
    private String pkg;

    public PkgNameLister(String mypkg) {
        pkg = mypkg;
    }

    public List<String> visit(List<Source> sources) {
        List<String> strs = new ArrayList<>();
        for(Source s: sources) {
            List<String> res = visit(s);
            if(res != null)
                strs.addAll(res);
        }
        return strs;
    }

    @Override
    public List<String> visit(Source ctx) {
        List<String> strs = new ArrayList<>();
        for(Stat st: ctx.stats) {
            List<String> res = visit(st);
            if(res != null)
                strs.addAll(res);
        }
        return strs;
    }

    @Override
    public List<String> visit(Pkg ctx) {
        List<String> strs = new ArrayList<>();
        for(Stat st: ctx.stats) {
            List<String> res = visit(st);
            if(res != null)
                strs.addAll(res);
        }
        return strs;
    }

    @Override
    public List<String> visit(Class ctx) {
        List<String> strs = new ArrayList<>();
        String name = ctx.name.toString();
        if(name.startsWith(pkg)) {
            if(name.compareTo(pkg) == 0) {
                strs.add(name);
            } else if(!name.substring(pkg.length()+1).contains(".")) {
                strs.add(name);
            }
        }
        if(ctx.template != null) {
            for (Stat st : ctx.template.stats) {
                List<String> res = visit(st);
                if (res != null)
                    strs.addAll(res);
            }
        }
        return strs;
    }

    @Override
    public List<String> visit(Trait ctx) {
        List<String> strs = new ArrayList<>();
        String name = ctx.name.toString();
        if(name.startsWith(pkg)) {
            if(name.compareTo(pkg) == 0) {
                strs.add(name);
            } else if(!name.substring(pkg.length()+1).contains(".")) {
                strs.add(name);
            }
        }
        if(ctx.template != null) {
            for (Stat st : ctx.template.stats) {
                List<String> res = visit(st);
                if (res != null)
                    strs.addAll(res);
            }
        }
        return strs;
    }

    @Override
    public List<String> visit(Object ctx) {
        List<String> strs = new ArrayList<>();
        String name = ctx.name.toString();
        if(name.startsWith(pkg)) {
            if(name.compareTo(pkg) == 0) {
                strs.add(name);
            } else if(!name.substring(pkg.length()+1).contains(".")) {
                strs.add(name);
            }
        }
        if(ctx.template != null) {
            for (Stat st : ctx.template.stats) {
                List<String> res = visit(st);
                if (res != null)
                    strs.addAll(res);
            }
        }
        return strs;
    }
}