/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

/*
 * This phase is the last phase in the compilation process: it creates .class files corresponding ot the AST.
 */

package eu.lepiller.scabo.transform;

import eu.lepiller.scabo.AstVisitor;
import eu.lepiller.scabo.scala.ast.Mod;
import eu.lepiller.scabo.scala.ast.Pkg;
import eu.lepiller.scabo.scala.ast.Source;
import eu.lepiller.scabo.scala.ast.Stat;
import eu.lepiller.scabo.scala.ast.defn.Class;

import eu.lepiller.scabo.scala.ast.defn.Trait;
import eu.lepiller.scabo.scala.ast.mod.Abstract;
import org.apache.bcel.Const;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;

import java.io.IOException;

public class BytecodeGenerator extends AstVisitor<String> {
    private ClassGen classgen;
    private ConstantPoolGen constantPoolGen;
    private String filename;

    @Override
    public String visit(Source s) {
        filename = s.filename;
        for (Stat st : s.stats)
            visit(st);
        return "";
    }

    @Override
    public String visit(Pkg p) {
        String ref = p.ref.toString();
        for (Stat s : p.stats)
            visit(s);
        return "";
    }

    public String visit(Class c) {
        System.err.println("Compiling class " + c.name.toString());
        int mods = Const.ACC_PUBLIC;
        for(Mod m: c.mods) {
            if(m instanceof Abstract)
                mods |= Const.ACC_ABSTRACT;
        }
        classgen = new ClassGen(c.name.toString(), "java.lang.Object", filename, mods, null);
        constantPoolGen = classgen.getConstantPool();

        // Add content
        if(c.template != null) {
            if(c.template.inits.size() > 0) {
                classgen.setSuperclassName(c.template.inits.get(0).toString());
            }
            for (Stat t : c.template.stats)
                visit(t);
        }

        JavaClass javaClass=classgen.getJavaClass();
        try {
            javaClass.dump(c.name.toString().replace(".", "/") + ".class");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String visit(Trait t) {
        System.err.println("Compiling trait " + t.name.toString());
        int mods = Const.ACC_PUBLIC | Const.ACC_INTERFACE;
        for(Mod m: t.mods)
            if(m instanceof Abstract)
                mods |= Const.ACC_ABSTRACT;

        classgen = new ClassGen(t.name.toString(), "java.lang.Object", filename, mods, null);
        constantPoolGen = classgen.getConstantPool();

        // Add content
        if(t.template != null) {
            if(t.template.inits.size() > 0) {
                classgen.setSuperclassName(t.template.inits.get(0).tpe.toString());
            }
            for (Stat st : t.template.stats)
                visit(st);
        }

        JavaClass javaClass=classgen.getJavaClass();
        try {
            javaClass.dump(t.name.toString().replace(".", "/") + ".class");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
