/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

package eu.lepiller.scabo;

import eu.lepiller.scabo.grammar.*;
import eu.lepiller.scabo.scala.ast.*;
import eu.lepiller.scabo.scala.ast.ctor.Secondary;
import eu.lepiller.scabo.scala.ast.defn.Def;
import eu.lepiller.scabo.scala.ast.defn.Object;
import eu.lepiller.scabo.scala.ast.defn.Trait;
import eu.lepiller.scabo.scala.ast.defn.Val;
import eu.lepiller.scabo.scala.ast.importee.Rename;
import eu.lepiller.scabo.scala.ast.importee.UnImport;
import eu.lepiller.scabo.scala.ast.importee.Wildcard;
import eu.lepiller.scabo.scala.ast.lit.*;
import eu.lepiller.scabo.scala.ast.lit.Boolean;
import eu.lepiller.scabo.scala.ast.lit.Double;
import eu.lepiller.scabo.scala.ast.lit.Long;
import eu.lepiller.scabo.scala.ast.mod.*;
import eu.lepiller.scabo.scala.ast.mod.Case;
import eu.lepiller.scabo.scala.ast.pat.*;
import eu.lepiller.scabo.scala.ast.pat.Var;
import eu.lepiller.scabo.scala.ast.term.*;
import eu.lepiller.scabo.scala.ast.term.Apply;
import eu.lepiller.scabo.scala.ast.term.ApplyInfix;
import eu.lepiller.scabo.scala.ast.term.Interpolate;
import eu.lepiller.scabo.scala.ast.term.Name;
import eu.lepiller.scabo.scala.ast.term.Param;
import eu.lepiller.scabo.scala.ast.term.Placeholder;
import eu.lepiller.scabo.scala.ast.term.Ref;
import eu.lepiller.scabo.scala.ast.term.Repeated;
import eu.lepiller.scabo.scala.ast.term.Select;
import eu.lepiller.scabo.scala.ast.term.Tuple;
import eu.lepiller.scabo.scala.ast.type.*;

import eu.lepiller.scabo.scala.ast.type.Annotate;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.lang.Override;

import java.lang.String;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ScalaASTBuilder extends ScalaBaseVisitor<Tree> {
    @Override
    public Tree visitCompilationUnit(ScalaParser.CompilationUnitContext ctx) {
        if(ctx.qualId().size() == 0)
            return null;

        Pkg current = new Pkg();
        Pkg outPkg = current;

        for(int i=0; i<ctx.qualId().size(); i++) {
            current.ref = visitPackage(ctx.qualId().get(i));
            if(i != ctx.qualId().size() - 1) {
                Pkg d = new Pkg();
                current.stats.add(d);
                current = d;
            }
        }

        for(int i=0; i<ctx.topStatSeq().topStat().size(); i++) {
            if(ctx.topStatSeq().topStat().get(i) instanceof ScalaParser.EmptyStatContext)
                continue;
            current.stats.add((Stat)visit(ctx.topStatSeq().topStat().get(i)));
        }

        return outPkg;
    }

    private Ref visitPackageRec(LinkedList<String> ids, Ref old) {
        if(ids.size() == 0) {
            return old;
        } else {
            Select s = new Select();
            s.qual = old;
            s.name = new Name(ids.pop());
            return visitPackageRec(ids, s);
        }
    }

    private Ref visitPackage(ScalaParser.QualIdContext ctx) {
        LinkedList<String> qual = new LinkedList<>();
        for(TerminalNode i: ctx.Id()) {
            qual.add(i.getText());
        }
        Name n = new Name(qual.pop());
        return visitPackageRec(qual, n);
    }

    /*
     * Import-related visitors
     */

    @Override
    public Tree visitImport_(ScalaParser.Import_Context ctx) {
        Import imp = new Import();
        for(int i = 0; i<ctx.importExpr().size(); i++) {
            imp.importers.add((Importer) visit(ctx.importExpr().get(i)));
        }
        return imp;
    }

    @Override
    public Tree visitImportExpr(ScalaParser.ImportExprContext ctx) {
        if(ctx.wild != null) {
            Importer s = new Importer();
            s.ref = visitStableIdRef(ctx.stableId());
            s.importees.add(new Wildcard());
            return s;
        } else if(ctx.importSelectors() != null) {
            Importer s = new Importer();
            s.ref = visitStableIdRef(ctx.stableId());
            ScalaParser.ImportSelectorsContext c = ctx.importSelectors();
            if(c instanceof ScalaParser.ImportNormalSelectorsContext) {
                ScalaParser.ImportNormalSelectorsContext ct = (ScalaParser.ImportNormalSelectorsContext)c;
                for(int i=0; i<ct.importSelector().size(); i++) {
                    s.importees.add((Importee)visit(ct.importSelector().get(i)));
                }
            } else {
                ScalaParser.ImportWildSelectorsContext ct = (ScalaParser.ImportWildSelectorsContext)c;
                for(int i=0; i<ct.importSelector().size(); i++) {
                    s.importees.add((Importee)visit(ct.importSelector().get(i)));
                }
                s.importees.add(new Wildcard());
            }
            return s;
        } else {
            Importer s = new Importer();
            Select r = (Select) visitStableIdRef(ctx.stableId());
            s.ref = (Ref) r.qual;
            s.importees.add(new eu.lepiller.scabo.scala.ast.importee.Name(r.name.toString()));
            return s;
        }
    }

    @Override
    public Tree visitImportNameSelector(ScalaParser.ImportNameSelectorContext ctx) {
        return new eu.lepiller.scabo.scala.ast.importee.Name(ctx.Id().getText());
    }

    @Override
    public Tree visitImportFunSelector(ScalaParser.ImportFunSelectorContext ctx) {
        Rename r = new Rename();
        r.name = new NameIndeterminate(ctx.Id().get(0).getText());
        r.rename = new NameIndeterminate(ctx.Id().get(0).getText());
        return r;
    }

    @Override
    public Tree visitImportWildSelector(ScalaParser.ImportWildSelectorContext ctx) {
        UnImport i = new UnImport();
        i.name = new NameIndeterminate(ctx.Id().getText());
        return i;
    }

    private Ref visitStableIdPrefixRef(ScalaParser.StableIdPrefixContext ctx) {
        Ref r;
        if (ctx.symthis != null) {
            Select s = new Select();
            This t = new This();
            if (ctx.before != null) {
                t.qual = new NameIndeterminate(ctx.before.getText());
            } else {
                t.qual = new NameAnonymous();
            }
            s.name = new NameIndeterminate(ctx.name.getText());
            r = s;
        } else if (ctx.symsuper != null) {
            Select s = new Select();
            Super t = new Super();
            if (ctx.before != null) {
                t.thisp = new NameIndeterminate(ctx.before.getText());
            } else {
                t.thisp = new NameAnonymous();
            }
            s.name = new NameIndeterminate(ctx.name.getText());
            if (ctx.classQualifier() != null)
                t.superp = new NameIndeterminate(ctx.classQualifier().Id().getText());
            else
                t.superp = new NameAnonymous();
            s.qual = t;
            r = s;
        } else {
            r = new Name(ctx.name.getText());
        }

        return r;
    }

    private Ref visitStableIdRef(ScalaParser.StableIdContext ctx) {
        Ref r;
        if (ctx.symthis != null) {
            Select s = new Select();
            This t = new This();
            if (ctx.before != null) {
                t.qual = new NameIndeterminate(ctx.before.getText());
            } else {
                t.qual = new NameAnonymous();
            }
            s.name = new NameIndeterminate(ctx.name.getText());
            r = s;
        } else if (ctx.symsuper != null) {
            Select s = new Select();
            Super t = new Super();
            if (ctx.before != null) {
                t.thisp = new NameIndeterminate(ctx.before.getText());
            } else {
                t.thisp = new NameAnonymous();
            }
            s.name = new NameIndeterminate(ctx.name.getText());
            if (ctx.classQualifier() != null)
                t.superp = new NameIndeterminate(ctx.classQualifier().Id().getText());
            else
                t.superp = new NameAnonymous();
            s.qual = t;
            r = s;
        } else {
            r = new Name(ctx.name.getText());
        }

        for(int i=1+(ctx.before != null?1:0); i<ctx.Id().size(); i++) {
            Select s = new Select();
            s.name = new Name(ctx.Id(i).getText());
            s.qual = r;
            r = s;
        }
        return r;
    }

    private Type visitStableIdType(ScalaParser.StableIdContext ctx) {
        Type r;
        if(ctx.symthis != null) {
            eu.lepiller.scabo.scala.ast.type.Select s = new eu.lepiller.scabo.scala.ast.type.Select();
            This t = new This();
            if(ctx.before != null) {
                t.qual = new NameIndeterminate(ctx.before.getText());
            } else {
                t.qual = new NameAnonymous();
            }
            s.name = new eu.lepiller.scabo.scala.ast.type.Name(ctx.name.getText());
            r = s;
        } else if(ctx.symsuper != null) {
            eu.lepiller.scabo.scala.ast.type.Select s = new eu.lepiller.scabo.scala.ast.type.Select();
            Super t = new Super();
            if(ctx.before != null) {
                t.thisp = new NameIndeterminate(ctx.before.getText());
            } else {
                t.thisp = new NameAnonymous();
            }
            s.name = new eu.lepiller.scabo.scala.ast.type.Name(ctx.name.getText());
            if(ctx.classQualifier() != null)
                t.superp = new NameIndeterminate(ctx.classQualifier().Id().getText());
            else
                t.superp = new NameAnonymous();
            s.qual = t;
            r = s;
        } else {
            r = new eu.lepiller.scabo.scala.ast.type.Name(ctx.name.getText());
        }

        for(int i=1+(ctx.before != null?1:0); i<ctx.Id().size(); i++) {
            eu.lepiller.scabo.scala.ast.type.Select s = new eu.lepiller.scabo.scala.ast.type.Select();
            s.name = new eu.lepiller.scabo.scala.ast.type.Name(ctx.Id(i).getText());
            if(r instanceof eu.lepiller.scabo.scala.ast.type.Select) {
                s.qual = new Select();
                ((Select) s.qual).name = new NameIndeterminate(((eu.lepiller.scabo.scala.ast.type.Select) r).name.value);
                s.qual = ((eu.lepiller.scabo.scala.ast.type.Select) r).qual;
            } else {
                s.qual = new Name(((eu.lepiller.scabo.scala.ast.type.Name) r).value);
            }
            r = s;
        }
        return r;
    }

    /*
     * TopStats visitors
     */

    @Override
    public Tree visitPackagingStat(ScalaParser.PackagingStatContext ctx) {
        Ref r = visitPackage(ctx.packaging().qualId());
        Pkg f = new Pkg();
        f.ref = r;
        for(int i = 0; i<ctx.packaging().topStatSeq().topStat().size(); i++) {
            f.stats.add((Stat)visit(ctx.packaging().topStatSeq().topStat().get(i)));
        }
        return f;
    }

    @Override
    public Tree visitPackageObjectStat(ScalaParser.PackageObjectStatContext ctx) {
        Object o = (Object)visit(ctx.packageObject().objectDef());
        PkgObject p = new PkgObject();
        p.name = o.name;
        p.mods = o.mods;
        p.tmpl = o.template;
        return p;
    }

    @Override
    public Tree visitDefStat(ScalaParser.DefStatContext ctx) {
        Defn d = (Defn)visit(ctx.tmplDef());
        if(d == null)
            return null;
        d.addMods(visitAnnotationMods(ctx.annotation()));
        d.addMods(visitModifiersMods(ctx.modifier()));
        return d;
    }

    private List<Mod> visitAnnotationMods(List<ScalaParser.AnnotationContext> ctxs) {
        List<Mod> mods = new ArrayList<>();
        for(ScalaParser.AnnotationContext ctx: ctxs) {
            mods.add((Mod) visit(ctx));
        }
        return mods;
    }

    private List<Mod> visitModifiersMods(List<ScalaParser.ModifierContext> ctxs) {
        List<Mod> mods = new ArrayList<>();
        for(ScalaParser.ModifierContext ctx: ctxs) {
            mods.add((Mod) visit(ctx));
        }
        return mods;
    }

    @Override
    public Tree visitModifierOverride(ScalaParser.ModifierOverrideContext ctx) {
        return new eu.lepiller.scabo.scala.ast.mod.Override();
    }

    @Override
    public Tree visitModifierLocal(ScalaParser.ModifierLocalContext ctx) {
        return visit(ctx.localModifier());
    }

    @Override
    public Tree visitLocalAbstractModifier(ScalaParser.LocalAbstractModifierContext ctx) {
        return new Abstract();
    }

    @Override
    public Tree visitLocalFinalModifier(ScalaParser.LocalFinalModifierContext ctx) {
        return new Final();
    }

    @Override
    public Tree visitLocalSealedModifier(ScalaParser.LocalSealedModifierContext ctx) {
        return new Sealed();
    }

    @Override
    public Tree visitLocalImplicitModifier(ScalaParser.LocalImplicitModifierContext ctx) {
        return new Implicit();
    }

    @Override
    public Tree visitLocalLazyModifier(ScalaParser.LocalLazyModifierContext ctx) {
        return new Lazy();
    }

    @Override
    public Tree visitModifierAccess(ScalaParser.ModifierAccessContext ctx) {
        return visit(ctx.accessModifier());
    }

    @Override
    public Tree visitAccessPrivate(ScalaParser.AccessPrivateContext ctx) {
        Private p = new Private();
        if (ctx.accessQualifier() != null) {
            if (ctx.accessQualifier().Id() != null) {
                p.within = new NameIndeterminate(ctx.accessQualifier().Id().getText());
            } else {
                p.within = new This();
            }
        }
        return p;
    }

    @Override
    public Tree visitAccessProtected(ScalaParser.AccessProtectedContext ctx) {
        Protected p = new Protected();
        if (ctx.accessQualifier() != null) {
            if (ctx.accessQualifier().Id() != null) {
                p.within = new NameIndeterminate(ctx.accessQualifier().Id().getText());
            } else {
                p.within = new This();
            }
        }
        return p;
    }

    /*
     * tmplDef
     */
    @Override
    public Tree visitTmplTraitDef(ScalaParser.TmplTraitDefContext ctx) {
        return visit(ctx.traitDef());
    }

    @Override
    public Tree visitTmplClassDef(ScalaParser.TmplClassDefContext ctx) {
        eu.lepiller.scabo.scala.ast.defn.Class c = (eu.lepiller.scabo.scala.ast.defn.Class) visit(ctx.classDef());
        if(ctx.is_case != null)
            c.mods.add(new Case());
        return c;
    }

    @Override
    public Tree visitTmplObjectDef(ScalaParser.TmplObjectDefContext ctx) {
        Object o = (Object)visit(ctx.objectDef());
        if(ctx.is_case != null)
            o.mods.add(new Case());
        return o;
    }

    @Override
    public Tree visitTraitDef(ScalaParser.TraitDefContext ctx) {
        Trait t = new Trait();
        t.name = new eu.lepiller.scabo.scala.ast.type.Name(ctx.Id().getText());
        if(ctx.typeParamClause() != null)
            t.tparams.addAll(visitTypeParamParam(ctx.typeParamClause().variantTypeParam()));
        t.template = (Template) visit(ctx.traitTemplateOpt());
        return t;
    }

    @Override
    public Tree visitTraitExtendTemplateOpt(ScalaParser.TraitExtendTemplateOptContext ctx) {
        return visit(ctx.traitTemplate());
    }

    @Override
    public Tree visitTraitNormalTemplateOpt(ScalaParser.TraitNormalTemplateOptContext ctx) {
        if(ctx.templateBody() == null)
            return null;
        return visit(ctx.templateBody());
    }

    @Override
    public Tree visitClassTemplate(ScalaParser.ClassTemplateContext ctx) {
        Template t;
        if(ctx.tmpl != null)
            t = (Template) visit(ctx.tmpl);
        else
            t = new Template();
        Init i = visitInit(ctx.classParents().constr());
        t.inits.add(i);
        for(ScalaParser.AnnotTypeContext c: ctx.classParents().annotType()) {
            t.inits.add((Init) visitInit(c));
        }
        return t;
    }

    @Override
    public Tree visitTraitTemplate(ScalaParser.TraitTemplateContext ctx) {
        Template t;
        if(ctx.tmpl != null)
            t = (Template) visit(ctx.tmpl);
        else
            t = new Template();
        for(ScalaParser.AnnotTypeContext c: ctx.traitParents().annotType()) {
            t.inits.add((Init) visitInit(c));
        }
        return t;
    }

    private Init visitInit(ScalaParser.AnnotTypeContext c) {
        Type t = (Type)visit(c);
        Init i = new Init();
        i.tpe = t;
        i.name = new NameAnonymous();
        return i;
    }

    private Init visitInit(ScalaParser.ConstrContext c) {
        Init i = new Init();
        i.tpe = (Type)visit(c.annotType());
        i.name = new NameAnonymous();
        i.argss = visitArgumentExprs(c.argumentExprs());
        return i;
    }

    @Override
    public Tree visitClassDef(ScalaParser.ClassDefContext ctx) {
        eu.lepiller.scabo.scala.ast.defn.Class c = new eu.lepiller.scabo.scala.ast.defn.Class();
        c.name = new eu.lepiller.scabo.scala.ast.type.Name(ctx.Id().getText());
        c.template = (Template) visit(ctx.classTemplateOpt());
        return c;
    }

    @Override
    public Tree visitObjectDef(ScalaParser.ObjectDefContext ctx) {
        Object o = new Object();
        o.name = new Name(ctx.Id().getText());
        o.template = (Template) visit(ctx.classTemplateOpt());
        return o;
    }

    @Override
    public Tree visitClassExtendTemplateOpt(ScalaParser.ClassExtendTemplateOptContext ctx) {
        return visit(ctx.classTemplate());
    }

    @Override
    public Tree visitClassNormalTemplateOpt(ScalaParser.ClassNormalTemplateOptContext ctx) {
        if (ctx.templateBody() != null) {
            return visit(ctx.templateBody());
        } else {
            return null;
        }
    }

    private List<eu.lepiller.scabo.scala.ast.type.Param> visitTypeParamParam(List<ScalaParser.VariantTypeParamContext> ctxs) {
        List<eu.lepiller.scabo.scala.ast.type.Param> params = new ArrayList<>();
        for(ScalaParser.VariantTypeParamContext ctx: ctxs) {
            eu.lepiller.scabo.scala.ast.type.Param p =
                    (eu.lepiller.scabo.scala.ast.type.Param) visit(ctx);
            if (ctx.annotation() != null) {
                p.mods.add((Mod) visit(ctx.annotation()));
            } if(ctx.variant != null) {
                if (ctx.variant.getType() == ScalaLexer.Minus)
                    p.mods.add(new Contravariant());
                else
                    p.mods.add(new Covariant());
            }
            // TODO
            params.add(p);
        }
        return params;
    }

    @Override
    public Tree visitAnnotation(ScalaParser.AnnotationContext ctx) {
        Annot a = new Annot();
        a.init = new Init();
        a.init.tpe = (Type) visit(ctx.simpleType());
        a.init.argss = visitArgumentExprs(ctx.argumentExprs());
        return a;
    }

    @Override
    public  Tree visitTypeParam(ScalaParser.TypeParamContext ctx) {
        eu.lepiller.scabo.scala.ast.type.Param p = new eu.lepiller.scabo.scala
                .ast.type.Param();
        if(ctx.Id() != null)
            p.name = new Name(ctx.Id().getText());
        else
            p.name = new NameAnonymous();
        if(ctx.typeParamClause() != null)
            p.tparams.addAll(visitTypeParamParam(ctx.typeParamClause().variantTypeParam()));
        if(ctx.lo != null)
            p.tbounds.lo = (Type) visit(ctx.lo);
        if(ctx.hi != null)
            p.tbounds.hi = (Type) visit(ctx.hi);
        if(ctx.vbounds() != null)
            for(ScalaParser.VboundsContext c: ctx.vbounds())
                p.vbounds.add((Type) visit(c.type()));
        if(ctx.cbounds() != null)
            for(ScalaParser.CboundsContext c: ctx.cbounds())
                p.cbounds.add((Type) visit(c.type()));
        return p;
    }

    @Override
    public Tree visitTemplateBody(ScalaParser.TemplateBodyContext ctx) {
        Template t = new Template();
        if(ctx.selfType() != null)
            t.self = (Self) visit(ctx.selfType());
        for (ScalaParser.TemplateStatContext s: ctx.templateStat()) {
            Stat st = (Stat) visit(s);
            if(st != null)
                t.stats.add(st);
        }
        return t;
    }

    @Override
    public Tree visitTemplateImportStat(ScalaParser.TemplateImportStatContext ctx) {
        return visit(ctx.import_());
    }

    @Override
    public Tree visitTemplateDefStat(ScalaParser.TemplateDefStatContext ctx) {
        Tree t = visit(ctx.def());
        if(t instanceof Secondary) {
            Secondary s = (Secondary) t;
            s.addMods(visitAnnotationMods(ctx.annotation()));
            s.addMods(visitModifiersMods(ctx.modifier()));
            return s;
        } else {
            Defn s = (Defn) t;
            s.addMods(visitAnnotationMods(ctx.annotation()));
            s.addMods(visitModifiersMods(ctx.modifier()));
            return s;
        }
    }

    @Override
    public Tree visitTemplateDclStat(ScalaParser.TemplateDclStatContext ctx) {
        Decl s = (Decl) visit(ctx.dcl());
        s.addMods(visitAnnotationMods(ctx.annotation()));
        s.addMods(visitModifiersMods(ctx.modifier()));
        return s;
    }

    @Override
    public Tree visitDclVal(ScalaParser.DclValContext ctx) {
        return visit(ctx.valDcl());
    }

    @Override
    public Tree visitDclVar(ScalaParser.DclVarContext ctx) {
        return visit(ctx.varDcl());
    }

    @Override
    public Tree visitDclFun(ScalaParser.DclFunContext ctx) {
        return visit(ctx.funDcl());
    }

    @Override
    public Tree visitDclType(ScalaParser.DclTypeContext ctx) {
        return visit(ctx.typeDcl());
    }

    @Override
    public Tree visitVarDcl(ScalaParser.VarDclContext ctx) {
        eu.lepiller.scabo.scala.ast.decl.Var v = new eu.lepiller.scabo.scala.ast.decl.Var();
        v.decltype = (Type) visit(ctx.type());
        for(TerminalNode n: ctx.ids().Id()) {
            Var var = new Var();
            var.name = new Name(n.getText());
            v.pats.add(var);
        }
        return v;
    }

    @Override
    public Tree visitTypeDcl(ScalaParser.TypeDclContext ctx) {
        eu.lepiller.scabo.scala.ast.decl.Type t = new eu.lepiller.scabo.scala.ast.decl.Type();
        t.name = new eu.lepiller.scabo.scala.ast.type.Name(ctx.Id().getText());
        if(ctx.typeParamClause() != null)
            t.tparams = visitTypeParamParam(ctx.typeParamClause().variantTypeParam());
        if(ctx.type().size() != 0) {
            t.bounds = new Bounds();
            if(ctx.lo != null)
                t.bounds.lo = (Type) visit(ctx.lo);
            if(ctx.hi != null)
                t.bounds.hi = (Type) visit(ctx.hi);
        }
        return t;
    }

    @Override
    public Tree visitValDcl(ScalaParser.ValDclContext ctx) {
        eu.lepiller.scabo.scala.ast.decl.Val v = new eu.lepiller.scabo.scala.ast.decl.Val();
        v.decltype = (Type) visit(ctx.type());
        for(TerminalNode t: ctx.ids().Id()) {
            Var var = new Var();
            var.name = new Name(t.getText());
            v.pats.add(var);
        }
        return v;
    }

    @Override
    public Tree visitFunDcl(ScalaParser.FunDclContext ctx) {
        eu.lepiller.scabo.scala.ast.decl.Def d = new eu.lepiller.scabo.scala.ast.decl.Def();

        d.name = new Name(ctx.funSig().operator().getText());
        if(ctx.funSig().funTypeParamClause() != null)
            for(ScalaParser.TypeParamContext p: ctx.funSig()
                    .funTypeParamClause().typeParam())
                d.tparams.add((eu.lepiller.scabo.scala.ast.type.Param) visit(p));
        ScalaParser.ParamClausesContext pctx = ctx.funSig().paramClauses();
        for(ScalaParser.ParamClauseContext ppctx: pctx.paramClause()) {
            List<Param> params = new ArrayList<>();
            if(ppctx.params() != null) {
                for (ScalaParser.ParamContext pc : ppctx.params().param()) {
                    params.add((Param) visit(pc));
                }
            }
            d.paramss.add(params);
        }

        if(pctx.params() != null) {
            List<Param> implicits = new ArrayList<>();
            for (ScalaParser.ParamContext ppctx : pctx.params().param()) {
                Param p = (Param) visit(ppctx);
                p.mods.add(new Implicit());
                implicits.add(p);
            }
            d.paramss.add(implicits);
        }

        return d;
    }

    @Override
    public Tree visitDefPatVar(ScalaParser.DefPatVarContext ctx) {
        return visit(ctx.patVarDef());
    }

    @Override
    public Tree visitDefVal(ScalaParser.DefValContext ctx) {
        Val v = new Val();
        v.rhs = (Term) visit(ctx.patDef().expr());
        if(ctx.patDef().type() != null) {
            v.decltype = (eu.lepiller.scabo.scala.ast.Type) visit(ctx.patDef().type());
        }
        for(ScalaParser.Pattern2Context p: ctx.patDef().pattern2()) {
            v.pats.add((Pat) visit(p));
        }
        return v;
    }

    @Override
    public Tree visitDefVar(ScalaParser.DefVarContext ctx) {
        return visit(ctx.varDef());
    }

    @Override
    public Tree visitDefType(ScalaParser.DefTypeContext ctx) {
        return visit(ctx.typeDef());
    }

    @Override
    public Tree visitTypeDef(ScalaParser.TypeDefContext ctx) {
        eu.lepiller.scabo.scala.ast.defn.Type t = new eu.lepiller.scabo.scala.ast.defn.Type();
        t.name = new eu.lepiller.scabo.scala.ast.type.Name(ctx.Id().getText());
        t.body = (eu.lepiller.scabo.scala.ast.Type) visit(ctx.type());
        if(ctx.typeParamClause() != null)
            t.tparams.addAll(visitTypeParamParam(ctx.typeParamClause().variantTypeParam()));
        return t;
    }

    @Override
    public Tree visitDefFun(ScalaParser.DefFunContext ctx) {
        return visit(ctx.funDef());
    }

    @Override
    public Tree visitFunDefBlock(ScalaParser.FunDefBlockContext ctx) {
        Def d = new Def();
        d.name = new Name(ctx.funSig().operator().getText());
        if(ctx.funSig().funTypeParamClause() != null)
            for(ScalaParser.TypeParamContext p: ctx.funSig()
                    .funTypeParamClause().typeParam())
                d.tparams.add((eu.lepiller.scabo.scala.ast.type.Param) visit(p));
        ScalaParser.ParamClausesContext pctx = ctx.funSig().paramClauses();
        for(ScalaParser.ParamClauseContext ppctx: pctx.paramClause()) {
            List<Param> params = new ArrayList<>();
            if(ppctx.params() != null) {
                for (ScalaParser.ParamContext pc : ppctx.params().param()) {
                    params.add((Param) visit(pc));
                }
            }
            d.paramss.add(params);
        }

        if(pctx.params() != null) {
            List<Param> implicits = new ArrayList<>();
            for (ScalaParser.ParamContext ppctx : pctx.params().param()) {
                Param p = (Param) visit(ppctx);
                p.mods.add(new Implicit());
                implicits.add(p);
            }
            d.paramss.add(implicits);
        }

        d.body = (Term) visit(ctx.block());
        return d;
    }

    @Override
    public Tree visitFunDefParam(ScalaParser.FunDefParamContext ctx) {
        Secondary s = new Secondary();

        List<Param> params = new ArrayList<>();
        if(ctx.paramClause().params() != null) {
            for (ScalaParser.ParamContext pc : ctx.paramClause().params().param()) {
                params.add((Param) visit(pc));
            }
        }
        s.paramss.add(params);

        ScalaParser.ParamClausesContext pctx = ctx.paramClauses();
        for(ScalaParser.ParamClauseContext ppctx: pctx.paramClause()) {
            params = new ArrayList<>();
            for(ScalaParser.ParamContext pc: ppctx.params().param()) {
                params.add((Param) visit(pc));
            }
            s.paramss.add(params);
        }

        if(ctx.constrBlock() != null) {
            s.init = (Init) visit(ctx.constrBlock().selfInvocation());
            for(ScalaParser.BlockStatContext b: ctx.constrBlock().blockStat()) {
                s.stats.add((Stat) visit(b));
            }
        } else if(ctx.constrExpr().constrBlock() != null) {
            for(ScalaParser.BlockStatContext b: ctx.constrExpr().constrBlock().blockStat()) {
                s.stats.add((Stat) visit(b));
            }
        } else {
            s.init = (Init) visit(ctx.constrExpr().selfInvocation());
        }

        return s;
    }

    private List<List<Term>> visitArgumentExprs(List<ScalaParser.ArgumentExprsContext> aess) {
        List<List<Term>> l = new ArrayList<>();
        for(ScalaParser.ArgumentExprsContext ae: aess) {
            List<Term> s = new ArrayList<>();
            if (ae instanceof ScalaParser.ArgumentExprsBlockContext) {
                s.add((Block) visit(((ScalaParser.ArgumentExprsBlockContext) ae).blockExpr()));
            } else if (ae instanceof ScalaParser.ArgumentExprsFunContext) {
                if(((ScalaParser.ArgumentExprsFunContext) ae).exprs() != null) {
                    for (ScalaParser.ExprContext ec : ((ScalaParser.ArgumentExprsFunContext) ae).exprs().expr()) {
                        s.add((Term) visit(ec));
                    }
                }
                l.add(s);
            } else {
                for (ScalaParser.ExprContext ec : ((ScalaParser.ArgumentExprsRepeatContext) ae).exprs().expr()) {
                    s.add((Term) visit(ec));
                }
                Repeated r = new Repeated();
                r.expr = (Term) visit(((ScalaParser.ArgumentExprsRepeatContext) ae).postfixExpr());
                s.add(r);
                l.add(s);
            }
        }

        return l;
    }

    @Override
    public Tree visitSelfInvocation(ScalaParser.SelfInvocationContext ctx) {
        Init i = new Init();

        i.tpe = new Singleton();
        ((Singleton) i.tpe).ref = new This();

        i.name = new NameAnonymous();

        i.argss = visitArgumentExprs(ctx.argumentExprs());

        return i;
    }

    @Override
    public Tree visitFunDefNormal(ScalaParser.FunDefNormalContext ctx) {
        Def d = new Def();
        d.name = new Name(ctx.funSig().operator().getText());
        if(ctx.funSig().funTypeParamClause() != null)
            for(ScalaParser.TypeParamContext p: ctx.funSig()
                    .funTypeParamClause().typeParam())
                d.tparams.add((eu.lepiller.scabo.scala.ast.type.Param) visit(p));
        ScalaParser.ParamClausesContext pctx = ctx.funSig().paramClauses();
        for(ScalaParser.ParamClauseContext ppctx: pctx.paramClause()) {
            List<Param> params = new ArrayList<>();
            if(ppctx.params() != null) {
                for (ScalaParser.ParamContext pc : ppctx.params().param()) {
                    params.add((Param) visit(pc));
                }
            }
            d.paramss.add(params);
        }

        if(pctx.params() != null) {
            List<Param> implicits = new ArrayList<>();
            for (ScalaParser.ParamContext ppctx : pctx.params().param()) {
                Param p = (Param) visit(ppctx);
                p.mods.add(new Implicit());
                implicits.add(p);
            }
            d.paramss.add(implicits);
        }

        if(ctx.type() != null)
            d.decltype = (Type) visit(ctx.type());

        d.body = (Term) visit(ctx.expr());
        return d;
    }

    @Override
    public Tree visitParam(ScalaParser.ParamContext ctx) {
        Param p = new Param();
        p.mods.addAll(visitAnnotationMods(ctx.annotation()));
        p.name = new Name(ctx.Id().getText());
        if(ctx.paramType() != null)
            p.decltype = (Type) visit(ctx.paramType());
        if(ctx.expr() != null)
            p.def = (Term) visit(ctx.expr());
        return p;
    }

    @Override
    public Tree visitVarDefPat(ScalaParser.VarDefPatContext ctx) {
        eu.lepiller.scabo.scala.ast.defn.Var v = new eu.lepiller.scabo.scala.ast.defn.Var();
        v.rhs = (Term) visit(ctx.patDef().expr());
        if(ctx.patDef().type() != null) {
            v.decltype = (Type) visit(ctx.patDef().type());
        }
        for(ScalaParser.Pattern2Context p: ctx.patDef().pattern2()) {
            v.pats.add((Pat) visit(p));
        }
        return v;
    }

    @Override
    public Tree visitVarDefUndef(ScalaParser.VarDefUndefContext ctx) {
        eu.lepiller.scabo.scala.ast.defn.Var v = new eu.lepiller.scabo.scala.ast.defn.Var();
        v.decltype = (Type) visit(ctx.type());
        for(TerminalNode t: ctx.ids().Id()) {
            Var p = new Var();
            p.name = new Name(t.getText());
            v.pats.add(p);
        }
        return v;
    }

    @Override
    public Tree visitVarPattern2(ScalaParser.VarPattern2Context ctx) {
        Bind b = new Bind();
        Var t = new Var();
        t.name = new Name(ctx.Id().getText());
        b.lhs = t;
        b.rhs = (Pat) visit(ctx.pattern3());
        return b;
    }

    @Override
    public Tree visitPattern23(ScalaParser.Pattern23Context ctx) {
        return visit(ctx.pattern3());
    }

    @Override
    public Tree visitPattern3(ScalaParser.Pattern3Context ctx) {
        if(ctx.operator().size() != 0) {
            ExtractInfix e = new ExtractInfix();
            for(int i=ctx.operator().size()-1; i>=0; i--) {
                if(ctx.simplePattern(i+1) instanceof ScalaParser.SimplePatternPatternContext) {
                    for (ScalaParser.PatternContext pc : ((ScalaParser.SimplePatternPatternContext) ctx.simplePattern(i+1)).patterns().pattern()) {
                        e.rhs.add((Pat) visit(pc));
                    }
                } else {
                    Pat p = (Pat) visit(ctx.simplePattern(i + 1));
                    e.rhs.add(p);
                }
                e.op = new Name(ctx.operator(i).getText());

                ExtractInfix n = new ExtractInfix();
                n.rhs.add(e);
                e = n;
            }
            return e;
        } else {
            return visit(ctx.simplePattern(0));
        }
    }

    @Override
    public Tree visitSimpleWildPattern(ScalaParser.SimpleWildPatternContext ctx) {
        return new eu.lepiller.scabo.scala.ast.pat.Wildcard();
    }

    @Override
    public Tree visitSimpleVarPattern(ScalaParser.SimpleVarPatternContext ctx) {
        return visitStableIdRef(ctx.stableId());
    }

    @Override
    public Tree visitSimpleLiteralPattern(ScalaParser.SimpleLiteralPatternContext ctx) {
        return visit(ctx.literal());
    }

    @Override
    public Tree visitExprExpr1(ScalaParser.ExprExpr1Context ctx) {
        return visit(ctx.expr1());
    }

    @Override
    public Tree visitExpr1PostfixAscription(ScalaParser.Expr1PostfixAscriptionContext ctx) {
        if(ctx.ascription() instanceof ScalaParser.AscriptionAscribeContext) {
            Ascribe a = new Ascribe();
            a.expr = (Term) visit(ctx.postfixExpr());
            a.tpe = (Type) visit(((ScalaParser.AscriptionAscribeContext) ctx.ascription()).infixType());
            return a;
        } else if(ctx.ascription() instanceof ScalaParser.AscriptionAnnotContext) {
            eu.lepiller.scabo.scala.ast.term.Annotate a = new eu.lepiller.scabo.scala.ast.term.Annotate();
            a.expr = (Term) visit(ctx.postfixExpr());
            for(ScalaParser.AnnotationContext annot: ((ScalaParser.AscriptionAnnotContext) ctx.ascription()).annotation()) {
                a.annots.add((Annot) visit(annot));
            }
        }
        assert(false);
        return null;
    }

    @Override
    public Tree visitExpr1Postfix(ScalaParser.Expr1PostfixContext ctx) {
        return visit(ctx.postfixExpr());
    }

    @Override
    public Tree visitPostfixInfix(ScalaParser.PostfixInfixContext ctx) {
        if(ctx.Id() == null) {
            return visit(ctx.infixExpr());
        }
        // TODO
        return null;
    }

    @Override
    public Tree visitEta(ScalaParser.EtaContext ctx) {
        Eta e = new Eta();
        e.expr = (Term) visit(ctx.simpleExpr1());
        return e;
    }

    @Override
    public Tree visitInfixPrefix(ScalaParser.InfixPrefixContext ctx) {
        return visit(ctx.prefixExpr());
    }

    @Override
    public Tree visitInfixBoth(ScalaParser.InfixBothContext ctx) {
        ApplyInfix a = new ApplyInfix();
        a.op = new Name(ctx.operator().getText());
        a.lhs = (Term) visit(ctx.infixExpr(0));
        a.args.add((Term) visit(ctx.infixExpr(1)));
        return a;
    }

    @Override
    public Tree visitPrefixExpr(ScalaParser.PrefixExprContext ctx) {
        // TODO: PLUS, MINUS, TILDE, BANG
        if(ctx.simpleExpr1() != null) {
            return (Term) visit(ctx.simpleExpr1());
        } else if(ctx.blockExpr() != null) {
            return visit(ctx.blockExpr());
        } else if(ctx.templateBody() != null) {
            System.out.println("TODO: prefixExpr -> templateBody");
            return null;
        } else {
            Term t;
            if(ctx.classTemplate().tmpl == null) {
                New n = new New();
                n.init = (Init) visitInit(ctx.classTemplate().classParents().constr());
                t = n;
            } else {
                // TODO
                t = new NewAnonymous();
            }
            return t;
        }
    }

    @Override
    public Tree visitSimpleExpr1Type(ScalaParser.SimpleExpr1TypeContext ctx) {
        ApplyType a = new ApplyType();
        a.fun = (Term) visit(ctx.simpleExpr1());
        for(ScalaParser.TypesTypeContext tc: ctx.typeArgs().types().typesType()) {
            a.targs.add((Type) visit(tc));
        }
        return a;
    }

    @Override
    public Tree visitTypesType(ScalaParser.TypesTypeContext ctx) {
        if(ctx.paramType() != null)
            return visit(ctx.paramType());
        return visit(ctx.typeParam());
    }

    @Override
    public Tree visitSimpleExpr1Dot(ScalaParser.SimpleExpr1DotContext ctx) {
        Select s = new Select();
        s.name = new Name(ctx.Id().getText());
        s.qual = (Term) visit(ctx.simpleExpr1());
        return s;
    }

    @Override
    public Tree visitSimpleExpr1Anon(ScalaParser.SimpleExpr1AnonContext ctx) {
        return new Placeholder();
    }
    @Override
    public Tree visitSimpleExpr1Multiple(ScalaParser.SimpleExpr1MultipleContext ctx) {
        if(ctx.exprs() == null)
            return new Unit();

        if(ctx.exprs().expr().size() == 1)
            return visit(ctx.exprs().expr(0));

        Tuple t = new Tuple();
        for(ScalaParser.ExprContext e: ctx.exprs().expr()) {
            t.args.add((Term) visit(e));
        }

        return t;
    }

    @Override
    public Tree visitBlockExprBlock(ScalaParser.BlockExprBlockContext ctx) {
        Block b = new Block();
        for(ScalaParser.BlockStatContext stat: ctx.block().blockStat()) {
            if(stat instanceof ScalaParser.BlockStatEmptyContext)
                continue;
            b.stats.add((Stat) visit(stat));
        }
        return b;
    }

    @Override
    public Tree visitExpr1PostfixMatch(ScalaParser.Expr1PostfixMatchContext ctx) {
        Match m = new Match();
        m.expr = (Term) visit(ctx.postfixExpr());
        for(ScalaParser.CaseClauseContext c: ctx.caseClauses().caseClause()) {
            m.cases.add((eu.lepiller.scabo.scala.ast.Case) visit(c));
        }
        return m;
    }

    @Override
    public Tree visitExpr1Try(ScalaParser.Expr1TryContext ctx) {
        Try t = new Try();
        if(ctx.block() != null)
            t.expr = (Term) visit(ctx.block());
        else
            t.expr = (Term) visit(ctx.expr(0));
        if(ctx.caseClauses() != null) {
            for (ScalaParser.CaseClauseContext c : ctx.caseClauses().caseClause()) {
                t.catchp.add((eu.lepiller.scabo.scala.ast.Case) visit(c));
            }
        }
        if(ctx.expr().size() == 2) {
            t.finallyp = (Term) visit(ctx.expr(1));
        }
        if(ctx.expr().size() == 1 && ctx.block() != null) {
            t.finallyp = (Term) visit(ctx.expr(0));
        }
        return t;
    }

    @Override
    public Tree visitCaseClause(ScalaParser.CaseClauseContext ctx) {
        eu.lepiller.scabo.scala.ast.Case c = new eu.lepiller.scabo.scala.ast.Case();
        c.body = (Term) visit(ctx.block());
        if(ctx.guard() != null)
            c.cond = (Term) visit(ctx.guard());
        c.pat = (Pat) visit(ctx.pattern());
        return c;
    }

    @Override
    public Tree visitBlock(ScalaParser.BlockContext ctx) {
        Block b = new Block();
        for(ScalaParser.BlockStatContext c: ctx.blockStat()) {
            b.stats.add((Stat) visit(c));
        }
        return b;
    }

    @Override
    public Tree visitPattern(ScalaParser.PatternContext ctx) {
        if(ctx.pattern1().size() == 1)
            return visit(ctx.pattern1(0));

        Alternative a = new Alternative();
        Alternative current = a;
        a.lhs = (Pat)visit(ctx.pattern1(0));
        for(int i=1; i<ctx.pattern1().size(); i++) {
            if(i == ctx.pattern1().size()-1) {
                current.rhs = (Pat) visit(ctx.pattern1(i));
            } else {
                Alternative n = new Alternative();
                current.rhs = n;
                n.lhs = (Pat) visit(ctx.pattern1(i));
                current = n;
            }
        }

        return a;
    }

    @Override
    public Tree visitPattern1Wild(ScalaParser.Pattern1WildContext ctx) {
        Typed t = new Typed();
        t.lhs = new eu.lepiller.scabo.scala.ast.pat.Wildcard();
        t.rhs = (Type) visit(ctx.typePat());
        return t;
    }

    @Override
    public Tree visitPattern1Type(ScalaParser.Pattern1TypeContext ctx) {
        Typed t = new Typed();
        t.lhs = new Name(ctx.Id().getText());
        t.rhs = (Type) visit(ctx.typePat());
        return t;
    }

    @Override
    public Tree visitPattern12(ScalaParser.Pattern12Context ctx) {
        return visit(ctx.pattern2());
    }

    @Override
    public Tree visitBlockStatExpr(ScalaParser.BlockStatExprContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Tree visitBlockStatDef(ScalaParser.BlockStatDefContext ctx) {
        Tree t = visit(ctx.def());
        if(t instanceof Secondary) {
            Secondary s = (Secondary) t;
            s.addMods(visitAnnotationMods(ctx.annotation()));
            if(ctx.mod != null) {
                if(ctx.mod.getText().compareTo("lazy") == 0)
                    s.addMod(new Lazy());
                if(ctx.mod.getText().compareTo("implicit") == 0)
                    s.addMod(new Implicit());
            }
            return s;
        } else {
            Defn s = (Defn) t;
            s.addMods(visitAnnotationMods(ctx.annotation()));
            if(ctx.mod != null) {
                if(ctx.mod.getText().compareTo("lazy") == 0)
                    s.addMod(new Lazy());
                if(ctx.mod.getText().compareTo("implicit") == 0)
                    s.addMod(new Implicit());
            }
            return s;
        }
    }

    @Override
    public Tree visitSimpleExpr1Arg(ScalaParser.SimpleExpr1ArgContext ctx) {
        Apply a;
        Tree args = visit(ctx.argumentExprs());
        if(args instanceof Apply) {
            a = (Apply) args;
        } else {
            a = new Apply();
            a.args.add((Block) args);
        }
        a.fun = (Term) visit(ctx.simpleExpr1());
        return a;
    }

    @Override
    public Tree visitArgumentExprsFun(ScalaParser.ArgumentExprsFunContext ctx) {
        Apply a = new Apply();
        if(ctx.exprs() == null)
            return a;
        for(ScalaParser.ExprContext expr: ctx.exprs().expr()) {
            a.args.add((Term) visit(expr));
        }
        return a;
    }

    @Override
    public Tree visitSimpleExpr1Literal(ScalaParser.SimpleExpr1LiteralContext ctx) {
        return visit(ctx.literal());
    }

    @Override
    public Tree visitSimpleExpr1StableId(ScalaParser.SimpleExpr1StableIdContext ctx) {
        return visitStableIdPrefixRef(ctx.stableIdPrefix());
    }

    @Override
    public Tree visitLiteralInt(ScalaParser.LiteralIntContext ctx) {
        String val = ctx.IntegerLiteral().getText();
        if(val.endsWith("L") || val.endsWith("l")) {
            Long i = new Long();
            if(val.startsWith("0x")) {
                i.value = java.lang.Long.parseLong(val.substring(2, val.length() - 1).toLowerCase(), 16);
            } else {
                i.value = java.lang.Long.parseLong(val.substring(0, val.length() - 1));
            }
            if(ctx.Minus() != null)
                i.value *= -1;
            return i;
        } else {
            Int i = new Int();
            if(val.startsWith("0x")) {
                val = val.substring(2);
                if(val.compareTo("80000000") > 0) {
                    Long ii = new Long();
                    ii.value = java.lang.Long.parseLong(val, 16);
                    return ii;
                }
                i.value = Integer.parseInt(val.toLowerCase(), 16);
            } else {
                i.value = Integer.parseInt(val);
            }
            if(ctx.Minus() != null)
                i.value *= -1;
            return i;
        }
    }

    @Override
    public Tree visitLiteralFloat(ScalaParser.LiteralFloatContext ctx) {
        Double i = new Double();
        i.value = java.lang.Double.parseDouble(ctx.FloatingPointLiteral().getText());
        return i;
    }

    @Override
    public Tree visitLiteralBool(ScalaParser.LiteralBoolContext ctx) {
        Boolean i = new Boolean();
        i.value = (ctx.BooleanLiteral().getText().compareTo("true") == 0);
        return i;
    }

    @Override
    public Tree visitLiteralChar(ScalaParser.LiteralCharContext ctx) {
        Char i = new Char();
        i.value = ctx.CharacterLiteral().getText().charAt(1);
        return i;
    }

    @Override
    public Tree visitLiteralString(ScalaParser.LiteralStringContext ctx) {
        eu.lepiller.scabo.scala.ast.lit.String i = new eu.lepiller.scabo.scala.ast.lit.String();
        i.value = ctx.StringLiteral().getText();
        if(i.value.startsWith("\"\"\"")) {
            i.value = i.value.substring(3, i.value.length() - 3);
        } else if(i.value.startsWith("\"")) {
                i.value = i.value.substring(1, i.value.length() - 1);
        } else {
            Interpolate interpolate = new Interpolate();
            String s = i.value;
            int pos = s.indexOf("\"");
            interpolate.prefix = new Name(s.substring(0, pos));

            s = s.substring(pos);
            if(s.startsWith("\"\"\"")) {
                s = s.substring(3, s.length()-3);
            } else {
                s = s.substring(1, s.length()-1);
            }

            CharStream input = CharStreams.fromString(s);
            InterpolateStringLexer lexer = new InterpolateStringLexer(input);
            InterpolateStringParser parser = new InterpolateStringParser(new CommonTokenStream(lexer));
            InterpolateStringParser.StringContext cst = parser.string();

            for(InterpolateStringParser.ArgContext a: cst.arg()) {
                interpolate.args.add(new Name(a.getText()));
            }
            if(cst.endarg() != null)
                interpolate.args.add(new Name(cst.endarg().getText()));
            for(InterpolateStringParser.PartContext a: cst.part()) {
                eu.lepiller.scabo.scala.ast.lit.String str = new eu.lepiller.scabo.scala.ast.lit.String();
                str.value = a.getText();
                interpolate.parts.add(str);
            }

            return interpolate;
        }

        return i;
    }

    @Override
    public Tree visitLiteralSymbol(ScalaParser.LiteralSymbolContext ctx) {
        Symbol i = new Symbol();
        i.value = ctx.SymbolLiteral().getText().substring(1);
        return i;
    }

    @Override
    public Tree visitLiteralNull(ScalaParser.LiteralNullContext ctx) {
        return new Null();
    }

    /*
     * Types
     */
    @Override
    public Tree visitTypePlaceholder(ScalaParser.TypePlaceholderContext ctx) {
        return new eu.lepiller.scabo.scala.ast.type.Placeholder();
    }

    @Override
    public Tree visitTypeFun(ScalaParser.TypeFunContext ctx) {
        eu.lepiller.scabo.scala.ast.type.Function f = new eu.lepiller.scabo.scala.ast.type.Function();
        f.res = (Type) visit(ctx.type());
        ScalaParser.FunctionArgTypesContext c = ctx.functionArgTypes();
        if(c instanceof ScalaParser.FunctionTypesInfixContext)
            f.params.add((Type) visit(((ScalaParser.FunctionTypesInfixContext) c).infixType()));
        else
            for(ScalaParser.ParamTypeContext cc: ((ScalaParser.FunctionTypesParamContext) c).paramType()) {
                f.params.add((Type) visit(cc));
            }
        return f;
    }

    @Override
    public Tree visitTypeInfix(ScalaParser.TypeInfixContext ctx) {
        if(ctx.existentialClause() == null)
            return visit(ctx.infixType());

        Existential e = new Existential();
        e.tpe = (Type) visit(ctx.infixType());
        for(ScalaParser.ExistentialDclContext c: ctx.existentialClause().existentialDcl()) {
            e.stats.add((Decl) visit(c));
        }
        return e;
    }

    private Type visitInfixCompoundTypes(Type lhs, LinkedList<Type> rest) {
        eu.lepiller.scabo.scala.ast.type.ApplyInfix a = new eu.lepiller.scabo.scala.ast.type.ApplyInfix();
        a.lhs = lhs;
        a.op = (eu.lepiller.scabo.scala.ast.type.Name) rest.pop();
        a.rhs = rest.pop();
        if(rest.size() == 0)
            return a;
        return visitInfixCompoundTypes(a, rest);
    }

    @Override
    public Tree visitInfixType(ScalaParser.InfixTypeContext ctx) {
        if(ctx.compoundType().size() == 1) {
            return visit(ctx.compoundType(0));
        }
        eu.lepiller.scabo.scala.ast.type.ApplyInfix lhs = new eu.lepiller.scabo.scala.ast.type.ApplyInfix();
        lhs.lhs = (Type) visit(ctx.compoundType(0));
        lhs.op = new eu.lepiller.scabo.scala.ast.type.Name(ctx.Id(0).getText());
        lhs.rhs = (Type) visit(ctx.compoundType(0));

        LinkedList<Type> rest = new LinkedList<>();
        for(int i=2; i<ctx.compoundType().size(); i++) {
            rest.add((Type) visit(ctx.compoundType(i)));
            rest.add(new eu.lepiller.scabo.scala.ast.type.Name(ctx.Id(i-1).getText()));
        }

        return visitInfixCompoundTypes(lhs, rest);
    }

    @Override
    public Tree visitCompoundTypeAnnot(ScalaParser.CompoundTypeAnnotContext ctx) {
        Type t;
        if(ctx.annotType().size() == 1) {
            t = (Type) visit(ctx.annotType(0));
        } else {
            With with = new With();
            t = with;
            for(int i = ctx.annotType().size()-1;i>0; i--) {
                with.rhs = (Type) visit(ctx.annotType(i));
                With tmp = new With();
                with.lhs = tmp;
                with = tmp;
            }
            with.lhs = (Type) visit(ctx.annotType(0));
        }

        if(ctx.refinement() != null) {
            Refine r = new Refine();
            r.tpe = t;
            for(ScalaParser.RefineStatContext c: ctx.refinement().refineStat()) {
                if(! (c instanceof ScalaParser.RefineEmptyContext))
                    r.stats.add((Stat) visit(c));
            }
            return r;
        } else {
            return t;
        }
    }

    @Override
    public Tree visitRefineDcl(ScalaParser.RefineDclContext ctx) {
        return visit(ctx.dcl());
    }

    @Override
    public Tree visitAnnotType(ScalaParser.AnnotTypeContext ctx) {
        if(ctx.annotation().size() == 0) {
            return visit(ctx.simpleType());
        } else {
            eu.lepiller.scabo.scala.ast.type.Annotate a = new eu.lepiller.scabo.scala.ast.type.Annotate();
            a.tpe = (Type) visit(ctx.simpleType());
            for(ScalaParser.AnnotationContext annot: ctx.annotation()) {
                a.annots.add((Annot) visit(annot));
            }
            return a;
        }
    }

    @Override
    public Tree visitSimpleTypeArgs(ScalaParser.SimpleTypeArgsContext ctx) {
        Type sim = (Type) visit(ctx.simpleType());
        eu.lepiller.scabo.scala.ast.type.Apply a = new eu.lepiller.scabo.scala.ast.type.Apply();
        a.tpe = sim;
        for(ScalaParser.TypesTypeContext c: ctx.typeArgs().types().typesType()) {
            a.args.add((Type) visit(c));
        }
        return a;
    }

    @Override
    public Tree visitSimpleTypeSharp(ScalaParser.SimpleTypeSharpContext ctx) {
        Project p = new Project();
        p.qual = (Type) visit(ctx.simpleType());
        p.name = new eu.lepiller.scabo.scala.ast.type.Name(ctx.Id().getText());
        return p;
    }

    @Override
    public Tree visitSimpleTypeType(ScalaParser.SimpleTypeTypeContext ctx) {
        Singleton s = new Singleton();
        if(ctx.stableId() != null) {
            s.ref = (Ref) visit(ctx.stableId());
        } else {
            This t = new This();
            if(ctx.Id() == null)
                t.qual = new NameAnonymous();
            else
                t.qual = new NameIndeterminate(ctx.Id().getText());
            s.ref = t;
        }
        return s;
    }

    @Override
    public Tree visitSimpleTypeTuple(ScalaParser.SimpleTypeTupleContext ctx) {
        eu.lepiller.scabo.scala.ast.type.Tuple t = new eu.lepiller.scabo.scala.ast.type.Tuple();
        for(ScalaParser.TypesTypeContext c: ctx.types().typesType()) {
            t.args.add((Type) visit(c));
        }
        return t;
    }

    @Override
    public Tree visitSimpleTypeStable(ScalaParser.SimpleTypeStableContext ctx) {
        return visitStableIdType(ctx.stableId());
    }

    @Override
    public Tree visitParamTypeType(ScalaParser.ParamTypeTypeContext ctx) {
        return visit(ctx.type());
    }

    @Override
    public Tree visitParamTypeByName(ScalaParser.ParamTypeByNameContext ctx) {
        ByName b = new ByName();
        b.tpe = (Type) visit(ctx.type());
        return b;
    }

    @Override
    public Tree visitParamTypeRepeated(ScalaParser.ParamTypeRepeatedContext ctx) {
        eu.lepiller.scabo.scala.ast.type.Repeated r = new eu.lepiller.scabo.scala.ast.type.Repeated();
        r.tpe = (Type) visit(ctx.type());
        return r;
    }
}
