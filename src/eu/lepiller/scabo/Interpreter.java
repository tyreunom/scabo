package eu.lepiller.scabo;

import eu.lepiller.scabo.scala.ast.*;

import java.util.List;

public class Interpreter {
    private List<Source> sources;

    public Interpreter(List<Source> s) {
        sources = s;
    }

    public Defn findDefinition(String name) {
        DefnFinder f = new DefnFinder(name);
        for(Source s: sources) {
            Defn n = f.visit(s);
            if(n != null)
                return n;
        }
        return null;
    }
}
