/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

package eu.lepiller.scabo;

import eu.lepiller.scabo.scala.ast.Source;
import eu.lepiller.scabo.scala.ast.Stat;
import eu.lepiller.scabo.scala.ast.Tree;

import eu.lepiller.scabo.grammar.ScalaParser;
import eu.lepiller.scabo.grammar.ScalaLexer;

import eu.lepiller.scabo.transform.BytecodeGenerator;
import eu.lepiller.scabo.GraphvizOutputter;

import eu.lepiller.scabo.Phase;
import eu.lepiller.scabo.transform.typer.ClassTypeResolver;
import eu.lepiller.scabo.transform.typer.TypeNameResolver;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Scabo {
    private static final int COMPILE = 0;
    private static final int DOT = 1;

    private static int mode = COMPILE;

    public static void main(String[] args) {
        boolean parse = true;
        List<String> files = new ArrayList<String>();

        for(String arg: args) {
            if(arg.startsWith("-") && parse) {
                if(arg.compareTo("--graph") == 0) {
                    mode = DOT;
                } else if(arg.compareTo("--") == 0) {
                    parse = false;
                } else {
                    System.err.println("Could not understand argument: " + arg);
                    usage();
                }
            } else {
                files.add(arg);
            }
        }

        if(files.size() < 1) {
            System.err.println("No source files were specified!");
            usage();
            System.exit(1);
        }

        compile(files);
    }

    private static void usage() {
        System.out.println("Usage: scabo [options] file_1.scala [file_n.scala ...]");
        System.out.println("Options:");
        System.out.println("  --graph: Print a representation of the AST in graphviz format.");
    }

    private static void showAST(List<Source> sources) {
        GraphvizOutputter graph = new GraphvizOutputter();
        System.out.println("digraph AST {");
        for(Source s: sources) {
            graph.visit(s);
        }
        System.out.println("}");
    }

    private static void compile(List<String> files) {
        List<Source> s = new ArrayList<>();
        for(String file: files) {
            Source source = parse(file);
            if (source == null)
                System.exit(1);
            String[] filename = file.split("/");
            source.filename = filename[filename.length - 1];
            s.add(source);
        }
        System.err.println("Done parsing everything!");

        if(mode == DOT) {
            showAST(s);
        }

        // Run phases in order
        List<Phase> phases = new ArrayList<>();
        phases.add(new ClassTypeResolver());
        phases.add(new TypeNameResolver());
        for(Phase p: phases) {
            s = p.run(s);
        }

        if(mode == DOT) {
            showAST(s);
        }

        // Finally, generate the bytecode
        BytecodeGenerator bcg = new BytecodeGenerator();
        for(Source source: s) {
            bcg.visit(source);
        }
    }

    /**
     * Parse a single source file
     * **/
    private static Source parse(String scalaFile) {
        System.err.println("Parsing " + scalaFile);
        try {
            CharStream input = CharStreams.fromFileName(scalaFile);
            ScalaLexer lexer = new ScalaLexer(input);
            ScalaParser parser = new ScalaParser(new CommonTokenStream(lexer));
            ScalaParser.CompilationUnitContext cst = parser.compilationUnit();
            Tree t = new ScalaASTBuilder().visitCompilationUnit(cst);
            Source s = new Source();
            s.stats.add((Stat)t);
            return s;
        } catch (Exception e) {
            System.err.println();
            System.err.println();
            System.err.println("Error while parsing " + scalaFile + ":");
            e.printStackTrace();
        }
        return null;
    }
}
