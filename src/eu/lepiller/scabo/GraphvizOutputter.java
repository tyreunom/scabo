/* This file is part of Scabo.
 *
 * Scabo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scabo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 *
 * You should have received a copy of the GNU General Public License
 * along with Scabo.  If not, see <https://www.gnu.org/licenses/>.
 * * * * * * */

package eu.lepiller.scabo;

import eu.lepiller.scabo.scala.ast.*;
import eu.lepiller.scabo.scala.ast.Case;
import eu.lepiller.scabo.scala.ast.Type;
import eu.lepiller.scabo.scala.ast.ctor.Primary;
import eu.lepiller.scabo.scala.ast.ctor.Secondary;
import eu.lepiller.scabo.scala.ast.defn.*;
import eu.lepiller.scabo.scala.ast.defn.Class;
import eu.lepiller.scabo.scala.ast.defn.Object;
import eu.lepiller.scabo.scala.ast.defn.Var;
import eu.lepiller.scabo.scala.ast.enumerator.Generator;
import eu.lepiller.scabo.scala.ast.enumerator.Guard;
import eu.lepiller.scabo.scala.ast.importee.Rename;
import eu.lepiller.scabo.scala.ast.importee.UnImport;
import eu.lepiller.scabo.scala.ast.importee.Wildcard;
import eu.lepiller.scabo.scala.ast.lit.*;
import eu.lepiller.scabo.scala.ast.lit.Boolean;
import eu.lepiller.scabo.scala.ast.lit.Double;
import eu.lepiller.scabo.scala.ast.lit.Float;
import eu.lepiller.scabo.scala.ast.lit.Short;
import eu.lepiller.scabo.scala.ast.lit.String;
import eu.lepiller.scabo.scala.ast.mod.*;
import eu.lepiller.scabo.scala.ast.pat.Alternative;
import eu.lepiller.scabo.scala.ast.pat.Bind;
import eu.lepiller.scabo.scala.ast.pat.Extract;
import eu.lepiller.scabo.scala.ast.pat.ExtractInfix;
import eu.lepiller.scabo.scala.ast.term.*;
import eu.lepiller.scabo.scala.ast.term.Annotate;
import eu.lepiller.scabo.scala.ast.term.ApplyInfix;
import eu.lepiller.scabo.scala.ast.term.Name;
import eu.lepiller.scabo.scala.ast.term.Placeholder;
import eu.lepiller.scabo.scala.ast.term.Repeated;
import eu.lepiller.scabo.scala.ast.term.Select;
import eu.lepiller.scabo.scala.ast.term.Tuple;
import eu.lepiller.scabo.scala.ast.type.*;
import eu.lepiller.scabo.scala.ast.type.Function;
import eu.lepiller.scabo.scala.ast.type.Param;

import java.lang.Long;
import java.lang.Override;
import java.util.List;

class GraphvizOutputter extends AstVisitor<Long> {
    private Long ID = new Long(0);

    private Long newID() {
        ID++;
        return ID;
    }

    /******************************************************************************************************************
     *                                            Top-level classes                                                   *
     ******************************************************************************************************************/

    @Override
    public Long visit(Source s) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Source\", shape = box];");

        for (Stat st : s.stats) {
            Long stid = visit(st);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Pkg p) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Pkg\", shape = box];");

        if (p.ref != null) {
            Long refid = visit(p.ref);
            System.out.println("  \"" + id + "\" -> \"" + refid + "\"");
        }
        for (Stat st : p.stats) {
            Long stid = visit(st);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(PkgObject ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"PkgObject\", shape = box];");

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.name != null) {
            Long nameid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + nameid + "\"");
        }
        if (ctx.tmpl != null) {
            Long templateid = visit(ctx.tmpl);
            System.out.println("  \"" + id + "\" -> \"" + templateid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Self ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"PkgObject\", shape = box];");

        if (ctx.decltype != null) {
            Long templateid = visit(ctx.decltype);
            System.out.println("  \"" + id + "\" -> \"" + templateid + "\"");
        }
        if (ctx.name != null) {
            Long nameid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + nameid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Case ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Case\", shape = box]");

        if (ctx.pat != null) {
            Long mid = visit(ctx.pat);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.cond != null) {
            Long mid = visit(ctx.cond);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.body != null) {
            Long mid = visit(ctx.body);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }


        return id;
    }

    @Override
    public Long visit(Template ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Template\", shape = box];");
        for (Stat st : ctx.early) {
            Long stid = visit(st);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }
        for (Init i : ctx.inits) {
            Long iid = visit(i);
            System.out.println("  \"" + id + "\" -> \"" + iid + "\"");
        }
        if (ctx.self != null) {
            Long selfid = visit(ctx.self);
            System.out.println("  \"" + id + "\" -> \"" + selfid + "\"");
        }
        for (Stat st : ctx.stats) {
            Long stid = visit(st);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Import ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Import\", shape = box];");

        for (Importer i : ctx.importers) {
            Long stid = visit(i);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Importer ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Importer\", shape = box];");

        if (ctx.ref != null) {
            Long refid = visit(ctx.ref);
            System.out.println("  \"" + id + "\" -> \"" + refid + "\"");
        }

        for (Importee i : ctx.importees) {
            Long stid = visit(i);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Init ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Init\", shape = box];");
        if (ctx.tpe != null) {
            Long nid = visit(ctx.tpe);
            System.out.println("  \"" + id + "\" -> \"" + nid + "\"");
        }
        if (ctx.name != null) {
            Long nid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + nid + "\"");
        }
        for (List<Term> ts : ctx.argss) {
            for (Term t : ts) {
                Long nid = visit(t);
                System.out.println("  \"" + id + "\" -> \"" + nid + "\"");
            }
        }
        return id;
    }

    @Override
    public Long visit(NameAnonymous ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"NameAnonymous\", shape = box]");
        return id;
    }

    @Override
    public Long visit(NameIndeterminate ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"NameIndeterminate: " + ctx.value + "\", shape = box]");
        return id;
    }

    /******************************************************************************************************************
     *                                                   Ctor                                                         *
     ******************************************************************************************************************/

    @Override
    public Long visit(Primary ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Ctor.Primary\", shape = box]");

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (List<eu.lepiller.scabo.scala.ast.term.Param> params : ctx.paramss) {
            for (eu.lepiller.scabo.scala.ast.term.Param param : params) {
                Long mid = visit(param);
                System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
            }
        }

        return id;
    }

    @Override
    public Long visit(Secondary ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Ctor.Secondary\", shape = box]");

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.init != null) {
            Long iid = visit(ctx.init);
            System.out.println("  \"" + id + "\" -> \"" + iid + "\"");
        }

        for (List<eu.lepiller.scabo.scala.ast.term.Param> params : ctx.paramss) {
            for (eu.lepiller.scabo.scala.ast.term.Param param : params) {
                Long mid = visit(param);
                System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
            }
        }

        for (Stat st : ctx.stats) {
            Long stid = visit(st);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        return id;
    }

    /******************************************************************************************************************
     *                                                   Decl                                                         *
     ******************************************************************************************************************/

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.decl.Def ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Decl.Def " + ctx.name + "\", shape = box]");
        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (eu.lepiller.scabo.scala.ast.type.Param param : ctx.tparams) {
            Long mid = visit(param);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (List<eu.lepiller.scabo.scala.ast.term.Param> params : ctx.paramss) {
            for (eu.lepiller.scabo.scala.ast.term.Param param : params) {
                Long mid = visit(param);
                System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
            }
        }
        if (ctx.decltype != null) {
            Long tid = visit(ctx.decltype);
            System.out.println("  \"" + id + "\" -> \"" + tid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.decl.Type ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Decl.Type\", shape = box]");

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (eu.lepiller.scabo.scala.ast.type.Param param : ctx.tparams) {
            Long mid = visit(param);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if(ctx.bounds != null) {
            Long mid = visit(ctx.bounds);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.decl.Val ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Decl.Val\", shape = box]");

        if (ctx.decltype != null) {
            Long mid = visit(ctx.decltype);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Pat pat : ctx.pats) {
            Long mid = visit(pat);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.decl.Var ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Decl.Var\", shape = box]");

        if (ctx.decltype != null) {
            Long mid = visit(ctx.decltype);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Pat pat : ctx.pats) {
            Long mid = visit(pat);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    /******************************************************************************************************************
     *                                                   Defn                                                         *
     ******************************************************************************************************************/

    @Override
    public Long visit(Class ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Defn.Class\", shape = box];");

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.name != null) {
            Long nameid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + nameid + "\"");
        }
        if (ctx.template != null) {
            Long templateid = visit(ctx.template);
            System.out.println("  \"" + id + "\" -> \"" + templateid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Def ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Defn.Def\", shape = box]");
        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.name != null) {
            Long nameid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + nameid + "\"");
        }
        for (eu.lepiller.scabo.scala.ast.type.Param param : ctx.tparams) {
            Long mid = visit(param);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (List<eu.lepiller.scabo.scala.ast.term.Param> params : ctx.paramss) {
            for (eu.lepiller.scabo.scala.ast.term.Param param : params) {
                Long mid = visit(param);
                System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
            }
        }
        if (ctx.decltype != null) {
            Long tid = visit(ctx.decltype);
            System.out.println("  \"" + id + "\" -> \"" + tid + "\"");
        }
        if (ctx.body != null) {
            Long bid = visit(ctx.body);
            System.out.println("  \"" + id + "\" -> \"" + bid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Macro ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Defn.Macro " + ctx.name + "\", shape = box]");

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.name != null) {
            Long nameid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + nameid + "\"");
        }

        for (eu.lepiller.scabo.scala.ast.type.Param param : ctx.tparams) {
            Long mid = visit(param);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.decltype != null) {
            Long tid = visit(ctx.decltype);
            System.out.println("  \"" + id + "\" -> \"" + tid + "\"");
        }

        if (ctx.body != null) {
            Long bid = visit(ctx.body);
            System.out.println("  \"" + id + "\" -> \"" + bid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Object ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Defn.Object\", shape = box];");

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.name != null) {
            Long nameid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + nameid + "\"");
        }
        if (ctx.template != null) {
            Long templateid = visit(ctx.template);
            System.out.println("  \"" + id + "\" -> \"" + templateid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Trait ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Defn.Trait\", shape = box];");

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.name != null) {
            Long nameid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + nameid + "\"");
        }
        if (ctx.template != null) {
            Long templateid = visit(ctx.template);
            System.out.println("  \"" + id + "\" -> \"" + templateid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.defn.Type ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Defn.Type\", shape = box];");

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.name != null) {
            Long nameid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + nameid + "\"");
        }

        for (eu.lepiller.scabo.scala.ast.type.Param param : ctx.tparams) {
            Long mid = visit(param);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.body != null) {
            Long bid = visit(ctx.body);
            System.out.println("  \"" + id + "\" -> \"" + bid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Val ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Defn.Val\", shape = box]");
        if (ctx.decltype != null) {
            Long mid = visit(ctx.decltype);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (Pat pat : ctx.pats) {
            Long mid = visit(pat);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.rhs != null) {
            Long mid = visit(ctx.rhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Var ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Defn.Var\", shape = box]");
        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (Pat pat : ctx.pats) {
            Long mid = visit(pat);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.decltype != null) {
            Long tid = visit(ctx.decltype);
            System.out.println("  \"" + id + "\" -> \"" + tid + "\"");
        }
        if (ctx.rhs != null) {
            Long tid = visit(ctx.rhs);
            System.out.println("  \"" + id + "\" -> \"" + tid + "\"");
        }
        return id;
    }

    /******************************************************************************************************************
     *                                                Enumerator                                                      *
     ******************************************************************************************************************/

    @Override
    public Long visit(Generator ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Enumerator.Generator\", shape = box];");
        if (ctx.pat != null) {
            Long tid = visit(ctx.pat);
            System.out.println("  \"" + id + "\" -> \"" + tid + "\"");
        }
        if (ctx.rhs != null) {
            Long tid = visit(ctx.rhs);
            System.out.println("  \"" + id + "\" -> \"" + tid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Guard ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Enumerator.Generator\", shape = box];");

        if (ctx.cond != null) {
            Long tid = visit(ctx.cond);
            System.out.println("  \"" + id + "\" -> \"" + tid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.enumerator.Val ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Enumerator.Generator\", shape = box];");

        if (ctx.pat != null) {
            Long tid = visit(ctx.pat);
            System.out.println("  \"" + id + "\" -> \"" + tid + "\"");
        }

        if (ctx.rhs != null) {
            Long tid = visit(ctx.rhs);
            System.out.println("  \"" + id + "\" -> \"" + tid + "\"");
        }

        return id;
    }

    /******************************************************************************************************************
     *                                                 Importee                                                       *
     ******************************************************************************************************************/

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.importee.Name ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Importee.Name: " + ctx.name.toString() + "\", shape = box];");
        return id;
    }

    @Override
    public Long visit(Rename ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Importee.Rename\", shape = box];");
        Long nid = visit(ctx.name);
        System.out.println("  \"" + id + "\" -> \"" + nid + "\"");
        Long rid = visit(ctx.rename);
        System.out.println("  \"" + id + "\" -> \"" + rid + "\"");
        return id;
    }

    @Override
    public Long visit(UnImport ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Importee.Unimport: " + ctx.name.toString() + "\", shape = box];");
        return id;
    }

    @Override
    public Long visit(Wildcard ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Importee.Wildcard\", shape = box];");
        return id;
    }

    /******************************************************************************************************************
     *                                                   Lit                                                          *
     ******************************************************************************************************************/

    @Override
    public Long visit(Boolean ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Lit.Boolean: '" + ctx.value + "'\", shape = box]");
        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.lit.Byte ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Lit.Byte: '" + ctx.value + "'\", shape = box]");
        return id;
    }

    @Override
    public Long visit(Char ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Lit.Char: '" + ctx.value + "'\", shape = box]");
        return id;
    }

    @Override
    public Long visit(Double ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Lit.Double: '" + ctx.value + "'\", shape = box]");
        return id;
    }

    @Override
    public Long visit(Float ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Lit.Float: '" + ctx.value + "'\", shape = box]");
        return id;
    }

    @Override
    public Long visit(Int ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Lit.Int: " + ctx.value + "\", shape = box]");
        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.lit.Long ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Lit.Long: '" + ctx.value + "'\", shape = box]");
        return id;
    }

    @Override
    public Long visit(Null ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Lit.Null\", shape = box]");
        return id;
    }

    @Override
    public Long visit(Short ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Lit.Short: '" + ctx.value + "'\", shape = box]");
        return id;
    }

    @Override
    public Long visit(String ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Lit.String: '" + ctx.value + "'\", shape = box]");
        return id;
    }

    @Override
    public Long visit(Symbol ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Lit.Symbol: '" + ctx.value + "'\", shape = box]");
        return id;
    }

    @Override
    public Long visit(Unit ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Lit.Unit\", shape = box]");
        return id;
    }

    private Long  visitMod(java.lang.String type) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Mod." + type + "\", shape = box]");
        return id;
    }

    /******************************************************************************************************************
     *                                                   Mod                                                          *
     ******************************************************************************************************************/

    @Override
    public Long visit(Abstract ctx) {
        return visitMod("Abstract");
    }

    @Override
    public Long visit(Annot ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Mod.Annot\", shape = box]");

        if(ctx.init != null) {
            Long mid = visit(ctx.init);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.mod.Case ctx) {
        return visitMod("Case");
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.mod.Contravariant ctx) {
        return visitMod("Contravariant");
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.mod.Covariant ctx) {
        return visitMod("Covariant");
    }

    @Override
    public Long visit(Final ctx) {
        return visitMod("Final");
    }

    @Override
    public Long visit(Implicit ctx) {
        return visitMod("Implicit");
    }

    @Override
    public Long visit(Inline ctx) {
        return visitMod("Inline");
    }

    @Override
    public Long visit(Lazy ctx) {
        return visitMod("Lazy");
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.mod.Override ctx) {
        return visitMod("Override");
    }

    @Override
    public Long visit(Private ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Private\", shape = box]");

        if(ctx.within != null) {
            Long mid = visit(ctx.within);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Protected ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Protected\", shape = box]");

        if(ctx.within != null) {
            Long mid = visit(ctx.within);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Sealed ctx) {
        return visitMod("Sealed");
    }

    @Override
    public Long visit(ValParam ctx) {
        return visitMod("ValParam");
    }

    @Override
    public Long visit(VarParam ctx) {
        return visitMod("VarParam");
    }

    /******************************************************************************************************************
     *                                                   Pat                                                          *
     ******************************************************************************************************************/

    @Override
    public Long visit(Alternative ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Pat.Alternative\", shape = box]");

        if (ctx.lhs != null) {
            Long mid = visit(ctx.lhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.rhs != null) {
            Long mid = visit(ctx.rhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Bind ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Pat.Bind\", shape = box]");

        if (ctx.lhs != null) {
            Long mid = visit(ctx.lhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.rhs != null) {
            Long mid = visit(ctx.rhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Extract ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Pat.Extract\", shape = box]");

        if (ctx.fun != null) {
            Long mid = visit(ctx.fun);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Pat pat : ctx.args) {
            Long mid = visit(pat);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(ExtractInfix ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Pat.ExtractInfix\", shape = box]");

        if (ctx.lhs != null) {
            Long mid = visit(ctx.lhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.op != null) {
            Long mid = visit(ctx.op);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Pat pat : ctx.rhs) {
            Long mid = visit(pat);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.pat.Interpolate ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Pat.Interpolate\", shape = box]");

        if (ctx.prefix != null) {
            Long mid = visit(ctx.prefix);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Lit lit : ctx.parts) {
            Long mid = visit(lit);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Pat pat : ctx.args) {
            Long mid = visit(pat);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.pat.SeqWildcard ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Pat.SeqWildcard\", shape = box]");
        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.pat.Tuple ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Pat.Tuple\", shape = box]");

        for (Pat pat : ctx.args) {
            Long mid = visit(pat);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.pat.Typed ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Pat.Typed\", shape = box]");

        if (ctx.lhs != null) {
            Long mid = visit(ctx.lhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.rhs != null) {
            Long mid = visit(ctx.rhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.pat.Var ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Pat.Var\", shape = box]");

        if (ctx.name != null) {
            Long mid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.pat.Wildcard ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Pat.Wildcard\", shape = box]");
        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.pat.Xml ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Pat.Xml\", shape = box]");

        for (Lit lit : ctx.parts) {
            Long mid = visit(lit);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Pat pat : ctx.args) {
            Long mid = visit(pat);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    /******************************************************************************************************************
     *                                                   Term                                                         *
     ******************************************************************************************************************/

    @Override
    public Long visit(Annotate ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Annotate\", shape = box]");

        if (ctx.expr != null) {
            Long mid = visit(ctx.expr);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Annot arg : ctx.annots) {
            Long stid = visit(arg);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.term.Apply ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Apply\", shape = box]");

        if (ctx.fun != null) {
            Long mid = visit(ctx.fun);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (Term arg : ctx.args) {
            Long stid = visit(arg);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(ApplyInfix ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.ApplyInfix\", shape = box]");

        if (ctx.lhs != null) {
            Long mid = visit(ctx.lhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.op != null) {
            Long mid = visit(ctx.op);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Type arg : ctx.targs) {
            Long stid = visit(arg);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        for (Term arg : ctx.args) {
            Long stid = visit(arg);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(ApplyType ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.ApplyType\", shape = box]");

        if (ctx.fun != null) {
            Long mid = visit(ctx.fun);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (Type arg : ctx.targs) {
            Long stid = visit(arg);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(ApplyUnary ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.ApplyUnary\", shape = box]");

        if (ctx.op != null) {
            Long mid = visit(ctx.op);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.arg != null) {
            Long mid = visit(ctx.arg);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Ascribe ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Ascribe\", shape = box]");

        if (ctx.expr != null) {
            Long mid = visit(ctx.expr);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.tpe != null) {
            Long mid = visit(ctx.tpe);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Assign ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Assign\", shape = box]");

        if (ctx.lhs != null) {
            Long mid = visit(ctx.lhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.rhs != null) {
            Long mid = visit(ctx.rhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Block ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Block\", shape = box]");

        for (Stat st : ctx.stats) {
            Long stid = visit(st);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Do ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Do\", shape = box]");

        if (ctx.expr != null) {
            Long mid = visit(ctx.expr);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.body != null) {
            Long mid = visit(ctx.body);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Eta ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Eta\", shape = box]");

        if (ctx.expr != null) {
            Long mid = visit(ctx.expr);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(For ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.For\", shape = box]");

        for (Enumerator enumerator: ctx.enums) {
            Long stid = visit(enumerator);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        if (ctx.body != null) {
            Long mid = visit(ctx.body);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(ForYield ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.ForYield\", shape = box]");

        for (Enumerator enumerator: ctx.enums) {
            Long stid = visit(enumerator);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        if (ctx.body != null) {
            Long mid = visit(ctx.body);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.term.Function ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Function\", shape = box]");

        for (eu.lepiller.scabo.scala.ast.term.Param param: ctx.params) {
            Long stid = visit(param);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        if (ctx.body != null) {
            Long mid = visit(ctx.body);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(If ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.If\", shape = box]");

        if (ctx.cond != null) {
            Long mid = visit(ctx.cond);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.thenp != null) {
            Long mid = visit(ctx.thenp);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.elsep != null) {
            Long mid = visit(ctx.elsep);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Interpolate ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Interpolate\", shape = box]");

        if (ctx.prefix != null) {
            Long mid = visit(ctx.prefix);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Term part : ctx.parts) {
            Long stid = visit(part);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        for (Term arg : ctx.args) {
            Long stid = visit(arg);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Match ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Match\", shape = box]");

        if (ctx.expr != null) {
            Long mid = visit(ctx.expr);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Case c : ctx.cases) {
            Long mid = visit(c);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Name ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Name: " + ctx.value + "\", shape = box]");
        return id;
    }

    @Override
    public Long visit(New ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.New\", shape = box]");

        if (ctx.init != null) {
            Long nid = visit(ctx.init);
            System.out.println("  \"" + id + "\" -> \"" + nid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(NewAnonymous ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.NewAnonymous\", shape = box]");

        if (ctx.tmpl != null) {
            Long nid = visit(ctx.tmpl);
            System.out.println("  \"" + id + "\" -> \"" + nid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.term.Param ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Param\", shape = box]");
        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.name != null) {
            Long mid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.decltype != null) {
            Long mid = visit(ctx.decltype);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.def != null) {
            Long mid = visit(ctx.def);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(PartialFunction ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.PartialFunction\", shape = box]");
        for (Case cas: ctx.cases) {
            Long mid = visit(cas);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Placeholder ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Placeholder\", shape = box]");
        return id;
    }

    @Override
    public Long visit(Repeated ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Repeated\", shape = box]");
        if (ctx.expr != null) {
            Long mid = visit(ctx.expr);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Return ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Return\", shape = box]");
        if (ctx.expr != null) {
            Long mid = visit(ctx.expr);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Select ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Select\", shape = box]");
        if (ctx.name != null) {
            Long mid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.qual != null) {
            Long mid = visit(ctx.qual);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Super ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Super\", shape = box]");

        if (ctx.thisp != null) {
            Long mid = visit(ctx.thisp);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.superp != null) {
            Long mid = visit(ctx.superp);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(This ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.This\", shape = box]");

        if (ctx.qual != null) {
            Long mid = visit(ctx.qual);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Throw ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Throw\", shape = box]");
        if (ctx.expr != null) {
            Long mid = visit(ctx.expr);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Try ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Try\", shape = box]");

        if (ctx.expr != null) {
            Long mid = visit(ctx.expr);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Case c : ctx.catchp) {
            Long mid = visit(c);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.finallyp != null) {
            Long mid = visit(ctx.finallyp);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(TryWithHandler ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.TryWithHandler\", shape = box]");
        if (ctx.expr != null) {
            Long mid = visit(ctx.expr);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.catchp != null) {
            Long mid = visit(ctx.catchp);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.finallyp != null) {
            Long mid = visit(ctx.finallyp);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Tuple ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.Tuple\", shape = box]");

        for (Term arg : ctx.args) {
            Long stid = visit(arg);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(While ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.While\", shape = box]");
        if (ctx.expr != null) {
            Long mid = visit(ctx.expr);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.body != null) {
            Long mid = visit(ctx.body);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(XML ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Term.XML\", shape = box]");
        for (Lit arg : ctx.parts) {
            Long stid = visit(arg);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }
        for (Term arg : ctx.args) {
            Long stid = visit(arg);
            System.out.println("  \"" + id + "\" -> \"" + stid + "\"");
        }
        return id;
    }

    /******************************************************************************************************************
     *                                                   type                                                         *
     ******************************************************************************************************************/

    @Override
    public Long visit(And ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.And\", shape = box]");
        if (ctx.lhs != null) {
            Long mid = visit(ctx.lhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.rhs != null) {
            Long mid = visit(ctx.rhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.type.Annotate ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Annotate\", shape = box]");
        if (ctx.tpe != null) {
            Long mid = visit(ctx.tpe);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (Annot annot: ctx.annots) {
            Long mid = visit(annot);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.type.Apply ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Apply\", shape = box]");

        if (ctx.tpe != null) {
            Long mid = visit(ctx.tpe);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (Type type : ctx.args) {
            Long mid = visit(type);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.type.ApplyInfix ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.ApplyInfix\", shape = box]");
        if (ctx.lhs != null) {
            Long mid = visit(ctx.lhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.op != null) {
            Long mid = visit(ctx.op);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.rhs != null) {
            Long mid = visit(ctx.rhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Bounds ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Bounds\", shape = box]");

        if (ctx.hi != null) {
            Long bid = visit(ctx.hi);
            System.out.println("  \"" + id + "\" -> \"" + bid + "\"");
        }
        if (ctx.lo != null) {
            Long bid = visit(ctx.lo);
            System.out.println("  \"" + id + "\" -> \"" + bid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(ByName ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.ByName\", shape = box]");

        if (ctx.tpe != null) {
            Long mid = visit(ctx.tpe);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(Existential ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Existential\", shape = box]");
        if (ctx.tpe != null) {
            Long mid = visit(ctx.tpe);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (Stat stat : ctx.stats) {
            Long mid = visit(stat);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Function ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Function\", shape = box]");

        if (ctx.res != null) {
            Long tid = visit(ctx.res);
            System.out.println("  \"" + id + "\" -> \"" + tid + "\"");
        }

        for (Type type : ctx.params) {
            Long mid = visit(type);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(ImplicitFunction ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.ImplicitFunction\", shape = box]");
        if (ctx.res != null) {
            Long mid = visit(ctx.res);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (Type param : ctx.params) {
            Long mid = visit(param);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Lambda ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Lambda\", shape = box]");
        if (ctx.tpe != null) {
            Long mid = visit(ctx.tpe);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (Param param : ctx.tparams) {
            Long mid = visit(param);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Method ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Method\", shape = box]");
        if (ctx.tpe != null) {
            Long mid = visit(ctx.tpe);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        for (List<eu.lepiller.scabo.scala.ast.term.Param> params : ctx.paramss) {
            for (eu.lepiller.scabo.scala.ast.term.Param param : params) {
                Long mid = visit(param);
                System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
            }
        }
        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.type.Name ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Name: " + ctx.value + "\", shape = box]");
        return id;
    }

    @Override
    public Long visit(Or ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Or\", shape = box]");
        if (ctx.lhs != null) {
            Long mid = visit(ctx.lhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.rhs != null) {
            Long mid = visit(ctx.rhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Param ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Param\", shape = box]");

        for (Mod mod : ctx.mods) {
            Long mid = visit(mod);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.name != null) {
            Long bid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + bid + "\"");
        }

        for (eu.lepiller.scabo.scala.ast.type.Param param : ctx.tparams) {
            Long mid = visit(param);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        if (ctx.tbounds != null) {
            Long bid = visit(ctx.tbounds);
            System.out.println("  \"" + id + "\" -> \"" + bid + "\"");
        }

        for (Type type : ctx.vbounds) {
            Long mid = visit(type);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        for (Type type : ctx.cbounds) {
            Long mid = visit(type);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.type.Placeholder ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Placeholder\", shape = box]");
        if (ctx.bounds != null) {
            Long mid = visit(ctx.bounds);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Project ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Project\", shape = box]");
        if (ctx.name != null) {
            Long mid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.qual != null) {
            Long mid = visit(ctx.qual);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Refine ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Refine\", shape = box]");

        if (ctx.tpe != null) {
            Long tid = visit(ctx.tpe);
            System.out.println("  \"" + id + "\" -> \"" + tid + "\"");
        }

        for (Stat stat : ctx.stats) {
            Long mid = visit(stat);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.type.Repeated ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Repeated\", shape = box]");
        if (ctx.tpe != null) {
            Long mid = visit(ctx.tpe);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.type.Select ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Select\", shape = box]");
        if (ctx.name != null) {
            Long mid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.qual != null) {
            Long mid = visit(ctx.qual);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(Singleton ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Singleton\", shape = box]");

        if (ctx.ref != null) {
            Long mid = visit(ctx.ref);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.type.Tuple ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Tuple\", shape = box]");

        for (Type type : ctx.args) {
            Long mid = visit(type);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }

        return id;
    }

    @Override
    public Long visit(eu.lepiller.scabo.scala.ast.type.Var ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.Var\", shape = box]");
        if (ctx.name != null) {
            Long mid = visit(ctx.name);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }

    @Override
    public Long visit(With ctx) {
        Long id = newID();
        System.out.println("  \"" + id + "\" [label = \"Type.With\", shape = box]");
        if (ctx.lhs != null) {
            Long mid = visit(ctx.lhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        if (ctx.rhs != null) {
            Long mid = visit(ctx.rhs);
            System.out.println("  \"" + id + "\" -> \"" + mid + "\"");
        }
        return id;
    }
}
