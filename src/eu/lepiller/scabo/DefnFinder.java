package eu.lepiller.scabo;

import eu.lepiller.scabo.scala.ast.Defn;
import eu.lepiller.scabo.scala.ast.Pkg;
import eu.lepiller.scabo.scala.ast.Source;
import eu.lepiller.scabo.scala.ast.Stat;
import eu.lepiller.scabo.scala.ast.defn.Class;
import eu.lepiller.scabo.scala.ast.defn.Object;

public class DefnFinder extends AstVisitor<Defn> {
    private String pack;
    private String search;

    public DefnFinder(String s) {
        pack = "";
        search = s;
    }

    @Override
    public Defn visit(Source s) {
        for(Stat st: s.stats) {
            Defn d = visit(st);
            if(d != null)
                return d;
        }
        return null;
    }

    @Override
    public Defn visit(Pkg p) {
        String oldPack = pack;
        for(Stat st: p.stats) {
            pack = oldPack + (oldPack.isEmpty()? "": ".") + p.ref.toString();
            Defn d = visit(st);
            if(d != null)
                return d;
        }
        pack = oldPack;
        return null;
    }

    @Override
    public Defn visit(Object o) {
        if((pack + "." + o.name.toString()).compareTo(search) == 0)
            return o;
        return null;
    }

    @Override
    public Defn visit(Class c) {
        if((pack + "." + c.name.toString()).compareTo(search) == 0)
            return c;
        return null;
    }
}
