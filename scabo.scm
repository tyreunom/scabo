(use-modules
  ((guix licenses) #:prefix license:)
  (guix build-system ant)
  (guix packages)
  (guix gexp)
  (gnu packages java)
  (gnu packages uml)
  (more packages java))

(package
  (name "scabo")
  (version "dev")
  (source (local-file (dirname (current-filename)) #:recursive? #t))
  (build-system ant-build-system)
  (arguments
   `(#:jdk ,icedtea-8
     #:phases
     (modify-phases %standard-phases
       (add-before 'build 'generate-sources
         (lambda _
           (invoke "ant" "generate-sources")
           #t)))))
  (inputs
   `(("java-antlr4" ,java-antlr4)
     ("java-antlr4-runtime" ,java-antlr4-runtime)
     ("java-commons-bcel" ,java-commons-bcel)))
  (native-inputs
   `(("java-junit" ,java-junit)
     ("plantuml" ,plantuml)))
  (home-page "https://scabo.lepiller.eu")
  (synopsis "Scala interpreter")
  (description "Scabo, the Scala Bootstrap Project, is a scala interpreter
aimed at bootstrapping the scala compiler.  Its role is to interpret the
compiler on its own source to produce a complete binary scala compiler.")
  (license license:bsd-3))
