scabo
=====

Scabo (The Scala Bootstrap Project) is a re-implementation of a Scala compiler.

Why do you do that, are you crazy?
==================================

The short answer is yes.  The long answer is that, just like most of the
compilers these days, scala is not [bootstrappable](https://boostrappable.org).
This means that to build scala, you first need a scala compiler.  As someone
said, this is yogurt software:

>  recipe for yogurt: add yogurt to milk.

This project aims at being able to compile every scala source file in the
scala compiler source code repository, in order to build a complete scala
compiler executable.  This is crazy, but it's probably the only clean way
to bootstrap scala.

To this end, we try to keep the requirements to the minimum possible.  We don't
expect the compilation process to be in any way efficient.  Indeed, it could take
days and a lot of memory.  The resulting compiler however should be as fast as the
so-called “official” compiler.

Why don't you use XXX?
======================

There are already existing projects that are able to compile scala code.  None of
them is doing the right thing for bootstrapping.

*  Some only support a subset of scala and are unsuitable to build the compiler.
*  Some, like scala.js, transpile to a non-JVM language.  We try to keep as
   close as possible to the normal target.
*  Some, like scala-native, are complete re-implementations of the scala
   compiler that would have fitted our needs, if they didn't depend on scala...
*  Another suggestion is to decompile the .class files to .java source.  Our
   goal is not to have readable source, so the end result would be fine with
   us.  The issue is that .class files are the result of compilation... by the
   scala compiler.  The java compiler may also not always be able to type check
   java sources obtained in this way, such as with co- and contra-variant types.
*  Lastly, we could build a pre-2.0 version of scala and build every intermediate
   version until the current version.  This is not very future-proof, as
   this method creates packages that depend on unmaintained software.  Another
   drawback is that it requires more builds than simply building all releases:
   the scala compiler is re-bootstrapped very often using non-stable versions.
   This means that the chain from the first scala compiler to the current
   compiler is already hundreds if not thousands of versions long.  And it will
   only grow.  Furthermore, it seems that older versions of scala depended on
   non-free software.

So how do I use scabo?
======================

This section is a work-in-progress. It doesn't work yet.

Building scabo
--------------

To build scabo, you need a java compiler, ANTLR and apache bcel.  If you are a
user of the Guix package manager, you can use our scabo.scm file that contains the
recipe for the current version:

```
guix build -f scabo.scm
```

Otherwise, you can run the ant script with `ant`.  We do not bundle dependencies,
so it is your responsibility to provide them in the CLASSPATH environment
variable.  If the script doesn't find your jdk, you can use the JAVAC
variable to specify where to find the `javac` command.  Similarly, the ANTLR
variable can be used to specify where to find the `antlr` comand.  For instance:

```
ant -DJAVAC=javac -DANTLR=antlr4
```

Usage
-----

To compile one or more scala files, you can run:

```
scabo source_1.scala [source_n.scala ...]
```
