/*
 [The "BSD licence"]
 Copyright (c) 2014 Leonardo Lucena
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/*
   Derived from http://www.scala-lang.org/files/archive/spec/2.11/13-syntax-summary.html
   Initial version from https://github.com/antlr/grammars-v4/tree/master/scala
   Then modified:

   Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
 */

grammar Scala;

literal
   : Minus? IntegerLiteral       # literalInt
   | Minus? FloatingPointLiteral # literalFloat
   | BooleanLiteral              # literalBool
   | CharacterLiteral            # literalChar
   | StringLiteral               # literalString
   | SymbolLiteral               # literalSymbol
   | 'null'                      # literalNull
   ;

qualId
   : Id ('.' Id)*
   ;

ids
   : Id (',' Id)*
   ;

stableId
   : ((before=Id '.')? (symthis='this' | symsuper='super' classQualifier?) '.')? name=Id ('.' Id)*
   ;

stableIdPrefix
   : ((before=Id '.')? (symthis='this' | symsuper='super' classQualifier?) '.')? name=Id
   ;

classQualifier
   : '[' Id ']'
   ;

type
   : functionArgTypes '=>' type   # typeFun
   | infixType existentialClause? # typeInfix
   | '_'                          # typePlaceholder
   ;

functionArgTypes
   : infixType                                               # functionTypesInfix
   | '(' Semi* (paramType (',' Semi* paramType)*)? Semi* ')' # functionTypesParam
   ;

existentialClause
   : 'forSome' '{' existentialDcl (Semi existentialDcl)* '}'
   ;

existentialDcl
   : 'type' typeDcl # existentialType
   | 'val' valDcl   # existentialVal
   ;

infixType
   : compoundType (Id compoundType)*
   ;

compoundType
   : annotType ('with' annotType)* refinement? # compoundTypeAnnot
   | refinement                                # coumpoundTypeRefine
   ;

annotType
   : simpleType annotation*
   ;

simpleType
   : simpleType typeArgs                            # simpleTypeArgs
   | simpleType '#' Id                              # simpleTypeSharp
   | stableId                                       # simpleTypeStable
   | (stableId | (Id '.')? thisp='this') '.' 'type' # simpleTypeType
   | '(' Semi* types Semi* ')'                      # simpleTypeTuple
   ;

typeArgs
   : '[' types ']'
   ;

types
   : typesType (',' typesType)*
   ;

typesType
   : paramType | typeParam
   ;

refinement
   : '{' Semi* refineStat (Semi refineStat)* Semi* '}'
   ;

refineStat
   : dcl            # refineDcl
   | 'type' typeDef # refineType
   |                # refineEmpty
   ;

typePat
   : type
   ;

ascription
   : ':' infixType   # ascriptionAscribe
   | ':' annotation+ # ascriptionAnnot
   | ':' '_' Star    # ascriptionSeqWild
   ;

expr
   : (bindings | 'implicit'? Id | '_') '=>' block # exprFun
   | expr1                                        # exprExpr1
   ;

expr1
   : 'if' '(' expr ')' Semi* expr (Semi* 'else' Semi* expr)?                                 # expr1If
   | 'while' '(' expr ')' Semi* expr                                                         # expr1While
   | 'try' Semi* ('{' block '}' | expr) Semi* ('catch' ('{' Semi* caseClauses Semi* '}' | expr))? (Semi* 'finally' expr)?     # expr1Try
   | 'do' expr Semi* 'while' '(' expr ')'                                                    # expr1Do
   | 'for' ('(' Semi* enumerators Semi* ')' | '{' Semi* enumerators Semi* '}') Semi* ('yield' Semi*)? expr       # expr1For
   | 'throw' expr                                                                            # expr1Throw
   | 'return' expr?                                                                          # expr1Return
   | (('new' (classTemplate | templateBody) | blockExpr | simpleExpr1 '_'?) '.') Id '=' Semi* expr # expr1New
   | Id '=' Semi* expr                                                                       # expr1Assign
   | simpleExpr1 argumentExprs '=' Semi* expr                                                      # expr1Equal
   | postfixExpr                                                                             # expr1Postfix
   | postfixExpr ascription                                                                  # expr1PostfixAscription
   | postfixExpr 'match' '{' Semi* caseClauses '}'                                     # expr1PostfixMatch
   ;

postfixExpr
   : infixExpr Id?   # postfixInfix
   | simpleExpr1 '_' # eta
   ;

operator
   : Id | Star | Minus | Plus | Tilde | Bang | ActualOp | '|'
   ;

infixExpr
   : prefixExpr                         # infixPrefix
   | infixExpr Semi* operator Semi* infixExpr # infixBoth
   ;

prefixExpr
   : (Minus | Plus | Tilde | Bang)? ('new' (classTemplate | templateBody) | blockExpr | simpleExpr1)
   ;

simpleExpr1
   : literal                                                     # simpleExpr1Literal
   | stableIdPrefix                                              # simpleExpr1StableId
   | (Id '.')? 'this'                                            # simpleExpr1This
   | '_'                                                         # simpleExpr1Anon
   | '(' Semi* exprs? Semi* ')'                                  # simpleExpr1Multiple
   | simpleExpr1 Semi* '.' Semi* Id                              # simpleExpr1Dot
   | ('new' (classTemplate | templateBody) | blockExpr) '.' Id   # simpleExpr1New
   | simpleExpr1 typeArgs                                        # simpleExpr1Type
   | ('new' (classTemplate | templateBody) | blockExpr) typeArgs # simpleExpr1NewType
   | simpleExpr1 argumentExprs                                   # simpleExpr1Arg
   ;

exprs
   : expr (',' Semi* expr)*
   ;

argumentExprs
   : '(' Semi* exprs? (',' Semi)? Semi* ')'                        # argumentExprsFun
   | '(' Semi* (exprs ',' Semi*)? postfixExpr ':' '_' Star Semi* ')' # argumentExprsRepeat
   | Semi* blockExpr                                   # argumentExprsBlock
   ;

blockExpr
   : '{' Semi* caseClauses '}' # blockExprPartial
   | '{' block '}'       # blockExprBlock
   ;

block
   : blockStat (Semi blockStat)*
   ;

blockStat
   : import_                                # blockStatImport
   | annotation* mod=('implicit' | 'lazy')? def # blockStatDef
   | annotation* localModifier* tmplDef     # blockStatTmpl
   | expr                                   # blockStatExpr
   |                                        # blockStatEmpty
   ;

enumerators
   : generator (Semi+ generator)*
   ;

generator
   : pattern1 '<-' expr (Semi* guard | Semi pattern1 '=' expr)*
   ;

caseClauses
   : (caseClause) +
   ;

caseClause
   : 'case' Semi* pattern Semi* (guard Semi*)? '=>' block
   ;

guard
   : 'if' postfixExpr
   ;

pattern
   : pattern1 Semi* ('|' Semi* pattern1 Semi*)*
   ;

pattern1
   : Id ':' typePat  # pattern1Type
   | '_' ':' typePat # pattern1Wild
   | pattern2        # pattern12
   ;

pattern2
   : Id At pattern3 # varPattern2
   | pattern3       # pattern23
   ;

pattern3
   : simplePattern (operator Semi* simplePattern)*
   ;

simplePattern
   : '_'                                                             # simpleWildPattern
   | stableId                                                        # simpleVarPattern
   | literal                                                         # simpleLiteralPattern
   | stableId '(' Semi* patterns? Semi* ')'                          # simpleStablePattern
   | stableId '(' Semi* (patterns? ',' Semi*)? ((Id|wild='_') At)? '_' Star Semi* ')' # simpleStableVarPattern
   | '(' Semi* patterns? Semi* ')'                                   # simplePatternPattern
   //| xmlPattern
   ;

patterns
   : pattern (',' Semi* pattern)*
   ;

typeParamClause
   : '[' variantTypeParam (',' Semi* variantTypeParam)* ']'
   ;

funTypeParamClause
   : '[' typeParam (',' Semi* typeParam)* ']'
   ;

variantTypeParam
   : annotation? variant=(Plus | Minus)? typeParam
   ;

typeParam
   : (Id | '_') typeParamClause? ('>:' lo=type)? ('<:' hi=type)?
           vbounds* cbounds*
   ;

vbounds
   : '<%' type
   ;

cbounds
   : ':' type
   ;

paramClauses
   : (paramClause Semi*)* ('(' Semi* 'implicit' params Semi* ')')?
   ;

paramClause
   : '(' Semi* params? Semi* ')'
   ;

params
   : param (',' Semi* param)*
   ;

param
   : annotation* Id (':' paramType)? ('=' Semi* expr)?
   ;

paramType
   : type      # paramTypeType
   | '=>' type # paramTypeByName
   | type Star # paramTypeRepeated
   ;

classParamClauses
   : classParamClause* ('(' Semi* 'implicit' classParams Semi* ')')?
   ;

classParamClause
   : '(' Semi* classParams? Semi* ')'
   ;

classParams
   : classParam (',' Semi* classParam)*
   ;

classParam
   : annotation* modifier* ('val' | 'var')? Id ':' paramType ('=' Semi* expr)?
   ;

bindings
   : '(' Semi* (binding (',' Semi* binding)*)? Semi* ')'
   | binding
   ;

binding
   : (Id | '_') (':' type)?
   ;

modifier
   : localModifier  # modifierLocal
   | accessModifier # modifierAccess
   | 'override'     # modifierOverride
   ;

localModifier
   : 'abstract' # localAbstractModifier
   | 'final'    # localFinalModifier
   | 'sealed'   # localSealedModifier
   | 'implicit' # localImplicitModifier
   | 'lazy'     # localLazyModifier
   ;

accessModifier
   : 'private' accessQualifier?   # accessPrivate
   | 'protected' accessQualifier? # accessProtected
   ;

accessQualifier
   : '[' (Id | 'this') ']'
   ;

annotation
   : At simpleType argumentExprs* Semi*
   ;

constrAnnotation
   : At simpleType argumentExprs
   ;

templateBody
   : '{' Semi* selfType? templateStat (Semi templateStat)* '}'
   ;

templateStat
   : import_                     # templateImportStat
   | (annotation)* modifier* def # templateDefStat
   | (annotation)* modifier* dcl # templateDclStat
   | expr                        # templateExprStat
   |                             # templateEmptyStat
   ;

selfType
   : Id (':' type)? '=>'
   | 'this' ':' type '=>'
   ;

import_
   : 'import' importExpr (',' importExpr)*
   ;

importExpr
   : stableId ( | '.' (wild='_' | importSelectors))
   ;

importSelectors
   : '{' Semi* (importSelector ',' Semi*)* importSelector Semi* '}' # importNormalSelectors
   | '{' Semi* (importSelector ',' Semi*)* '_' Semi* '}'            # importWildSelectors
   ;

importSelector
   : Id            # importNameSelector
   | Id '=>' Id    # importFunSelector
   | Id '=>' '_'   # importWildSelector
   ;

dcl
   : 'val' valDcl   # dclVal
   | 'var' varDcl   # dclVar
   | 'def' funDcl   # dclFun
   | 'type' typeDcl # dclType
   ;

valDcl
   : ids ':' type
   ;

varDcl
   : ids ':' type
   ;

funDcl
   : funSig (':' type)?
   ;

funSig
   : operator funTypeParamClause? paramClauses
   ;

typeDcl
   : Id typeParamClause? ('>:' lo=type)? ('<:' hi=type)?
   ;

patVarDef
   : 'val' patDef # defVal
   | 'var' varDef # defVar
   ;

def
   : patVarDef      # defPatVar
   | 'def' funDef   # defFun
   | 'type' typeDef # defType
   | tmplDef        # defTmpl
   ;

patDef
   : pattern2 (',' pattern2)* (':' type)? Semi* '=' Semi* expr
   ;

varDef
   : patDef               # varDefPat
   | ids ':' type '=' Semi* '_' # varDefUndef
   ;

funDef
   : funSig (':' type)? '=' Semi* expr # funDefNormal
   | funSig '{' block '}'              # funDefBlock
   | 'this' paramClause paramClauses ('=' Semi* constrExpr | constrBlock) #funDefParam
   ;

typeDef
   : Id typeParamClause? '=' Semi* type
   ;

tmplDef
   : is_case='case'? 'class' classDef   # tmplClassDef
   | is_case='case'? 'object' objectDef # tmplObjectDef
   | 'trait' traitDef                   # tmplTraitDef
   ;

classDef
   : Id typeParamClause? constrAnnotation* accessModifier? classParamClauses Semi* classTemplateOpt
   ;

traitDef
   : Id typeParamClause? traitTemplateOpt
   ;

objectDef
   : Id classTemplateOpt
   ;

classTemplateOpt
   : 'extends' classTemplate        # classExtendTemplateOpt
   | (ext='extends'? templateBody)? # classNormalTemplateOpt
   ;

traitTemplateOpt
   : 'extends' traitTemplate        # traitExtendTemplateOpt
   | (ext='extends'? templateBody)? # traitNormalTemplateOpt
   ;

classTemplate
   : early=earlyDefs? classParents Semi* tmpl=templateBody?
   ;

traitTemplate
   : early=earlyDefs? traitParents tmpl=templateBody?
   ;

classParents
   : constr (Semi* 'with' annotType)*
   ;

traitParents
   : annotType (Semi* 'with' annotType)*
   ;

constr
   : annotType argumentExprs*
   ;

earlyDefs
   : '{' Semi* (earlyDef (Semi* earlyDef)*)? Semi* '}' 'with'
   ;

earlyDef
   : (annotation)* modifier* patVarDef
   ;

constrExpr
   : selfInvocation
   | constrBlock
   ;

constrBlock
   : '{' Semi* selfInvocation (Semi blockStat)* '}'
   ;

selfInvocation
   : 'this' argumentExprs +
   ;

topStatSeq
   : topStat (Semi topStat)*
   ;

topStat
   : (annotation)* modifier* tmplDef # defStat
   | import_                         # importStat
   | packaging                       # packagingStat
   | packageObject                   # packageObjectStat
   |                                 # emptyStat
   ;

packaging
   : 'package' qualId '{' topStatSeq '}'
   ;

packageObject
   : 'package' 'object' objectDef
   ;

compilationUnit
   : Semi* ('package' qualId Semi)* topStatSeq
   ;

// Lexer

BooleanLiteral
   : 'true' | 'false'
   ;


CharacterLiteral
   : '\'' (PrintableChar | CharEscapeSeq | UnicodeEscape) '\''
   ;


StringLiteral
   : Lower* '"' StringElement* '"' | Lower* '"""' .*? '"""'
   ;


SymbolLiteral
   : '\'' Plainid
   ;


IntegerLiteral
   : (DecimalNumeral | HexNumeral) ('L' | 'l')?
   ;


FloatingPointLiteral
   : Digit + '.' Digit + ExponentPart? FloatType? | '.' Digit + ExponentPart? FloatType? | Digit ExponentPart FloatType? | Digit + ExponentPart? FloatType
   ;


Id
   : Plainid | '`' StringElement* '`'
   ;

Varid
   : Lower Idrest
   ;


Semi
   : ';' | ';'? ([\n\r] | SINGLELINECOMMENT)+
   ;

WS
   : [ \t] -> skip
   ;



Paren
   : '(' | ')' | '[' | ']' | '{' | '}'
   ;


Delim
   : '`' | '\'' | '"' | '.' | ';' | ','
   ;

ActualOp
   : AugmentedOpchar AugmentedOpchar+
   ;



//Comment
//   : '/*' .*? '*/' | '//' .*? [\n\r]
//   ;

MULTILINECOMMENT
   : '/*' (MULTILINECOMMENT | .)*? '*/' -> skip
   ;

SINGLELINECOMMENT
   : '//' .*? [\n\r]
   ;

Minus
   : '-'
   ;

Plus
   : '+'
   ;

Tilde
   : '~'
   ;

Bang
   : '!'
   ;

Star
   : '*'
   ;

At
   : '@'
   ;

// fragments

fragment UnicodeEscape
   : '\\' 'u' 'u'? HexDigit HexDigit HexDigit HexDigit
   ;


fragment WhiteSpace
   : '\u0020' | '\u0009' | '\u000D' | '\u000A'
   ;


fragment Op
   : Opchar+
   ;


fragment Plainid
   : Upper Idrest | Varid | Op
   ;


fragment Idrest
   : (Letter | Digit)* ('_' AugmentedOp)?
   ;

fragment AugmentedOp
   : AugmentedOpchar+
   ;

fragment StringElement
   : '${' .*? '}' | '\u0020' | '\u0021' | '\u0023' .. '\u007F' | CharEscapeSeq | Unicode
   ;

fragment Unicode
   : '\u007f' .. '\uffff'
   ;

fragment MultiLineChars
   : ('"'? '"'? .*?)* '"'*
   ;


fragment HexDigit
   : '0' .. '9' | 'A' .. 'F' | 'a' .. 'f'
   ;


fragment FloatType
   : 'F' | 'f' | 'D' | 'd'
   ;


fragment Upper
   : 'A' .. 'Z' | '$' | '_'
   ;

// and Unicode category Lu

fragment Lower
   : 'a' .. 'z'
   ;

// and Unicode category Ll

fragment Letter
   : Upper | Lower
   ;

// and Unicode categories Lo, Lt, Nl

fragment ExponentPart
   : ('E' | 'e') (Plus | Minus)? Digit +
   ;


fragment PrintableChar
   : '\u0020' .. '\u007F'
   ;


fragment CharEscapeSeq
   : '\\' ('b' | 't' | 'n' | 'f' | 'r' | '"' | '\'' | '\\')
   ;


fragment DecimalNumeral
   : '0' | NonZeroDigit Digit*
   ;


fragment HexNumeral
   : '0' 'x' HexDigit HexDigit +
   ;


fragment Digit
   : '0' | NonZeroDigit
   ;


fragment NonZeroDigit
   : '1' .. '9'
   ;


// printableChar not matched by (whiteSpace | upper | lower |
// letter | digit | paren | delim | opchar | Unicode_Sm | Unicode_So
fragment Opchar
   //:  PrintableChar
   : '\u0023' .. '\u0026'
   | '\u002F'
   | '\u003A'
   | '\u003C' .. '\u003F'
   | '\u005C'
   | '\u005E'
   | '\u007C'
   | '\u007F'
   ;

fragment AugmentedOpchar
   : Opchar | Plus | Minus | Tilde | Bang | Star
   ;
