grammar InterpolateString;

string
   : (part arg)* (part | part? endarg)?
   ;

part
   : (PartChr | Escape | Space | CloseCurl | OpenCurl | Dot | ArgChr)+
   ;

arg
   : directArg
   | exprArg
   ;

directArg
   : Dollar (ArgChr)+ Space
   ;

exprArg
   : Dollar OpenCurl expr CloseCurl
   ;

expr
   : (ArgChr)+
   | expr Dot expr
   ;

endarg
   : Dollar (ArgChr)+
   ;

Dot
   : '.'
   ;

OpenCurl
   : '{'
   ;

CloseCurl
   : '}'
   ;

Dollar
   : '$'
   ;

Space
   : ' '
   ;

Escape
   : '\\' ('$' | '"' | '\\')
   ;

ArgChr
   : 'a' .. 'z' | '0' .. '9' | '_' | 'A' .. 'Z'
   ;

PartChr
   : '\u0021' .. '\u0023'
   | '\u0025' .. '\u002d'
   | '\u002f'
   | '\u003a' .. '\u0040'
   | '\u005b' .. '\u0060'
   | '\u007c'
   | '\u007e' .. '\uffff'
   ;